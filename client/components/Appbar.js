import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';

import MenuPage from './MenuPage';

const MenuAppBar = ({ notifs, auth }) => {
  return (
    <React.Fragment>
      <AppBar position="static" id="appBar">
        <Typography id="HomeName" variant="h5">
          M A T C H U S
        </Typography>
        <Toolbar id="Toolbar">
          <MenuPage notifs={notifs} authProps={auth} />
        </Toolbar>
      </AppBar>
    </React.Fragment>
  );
};

export default MenuAppBar;
