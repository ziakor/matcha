import React, { useState } from 'react';
import Grid from '@material-ui/core/Grid';
import { InputLabel } from '@material-ui/core';
import Input from '@material-ui/core/Input';
import FormControl from '@material-ui/core/FormControl';
import Button from '@material-ui/core/Button';
import { error } from 'next/dist/build/output/log';
import makeAnimated from 'react-select/animated';
import Select from 'react-select';

const options = [
  { value: 'Rap', label: 'Rap', color: '#000000' },
  { value: 'RnB', label: 'RnB', color: '#000000' },
  { value: 'Chiens', label: 'Chiens', color: '#000000' },
  { value: 'Chats', label: 'Chats', color: '#000000' },
  { value: 'Cinema', label: 'Cinema', color: '#000000' },
  { value: 'Tarantino', label: 'Tarantino', color: '#000000' },
  { value: 'Marvel', label: 'Marvel', color: '#000000' },
  { value: 'Skateboard', label: 'Skateboard', color: '#000000' },
  { value: 'Traveling', label: 'Traveling', color: '#000000' },
  { value: 'Paris', label: 'Paris', color: '#000000' },
  { value: 'Bordeaux', label: 'Bordeaux', color: '#000000' },
  { value: 'Foot', label: 'Foot', color: '#000000' },
  { value: 'Coding', label: 'Coding', color: '#000000' },
  { value: '42Born2Code', label: '42Born2Code', color: '#000000' },
  { value: 'Food', label: 'Food', color: '#000000' },
  { value: 'Sport', label: 'Sport', color: '#000000' },
  { value: 'Google', label: 'Google', color: '#000000' },
  { value: 'Beer', label: 'Beer', color: '#000000' },
  { value: 'Tuning', label: 'Tuning', color: '#000000' },
  { value: 'Pringles', label: 'Pringles', color: '#000000' }
];
const animatedComponents = makeAnimated();

const SettingsTags = ({ userTags }) => {
  const [bioForm, setBioForm] = useState({
    bio: '',
    tags: userTags.map((value, index) => ({
      value: value,
      label: value,
      color: '#000000'
    }))
  });
  const [bioFormStatus, setBioFormStatus] = useState({
    bio: true,
    tags: true
  });
  const [merror, setMerror] = useState('');
  const handleChangeTags = (values) => {
    setBioForm((oldvalues) => ({ ...oldvalues, ['tags']: values }));
    setBioFormStatus((oldvalues) => ({ ...oldvalues, ['tags']: true }));
  };
  const handleChangeBio = (event) => {
    event.persist();

    if (event.target.value.length > 160) {
      setBioForm((oldvalues) => ({
        ...oldvalues,
        ['bio']: event.target.value
      }));
      setBioFormStatus((oldvalues) => ({ ...oldvalues, ['bio']: true }));
    } else {
      setBioForm((oldvalues) => ({
        ...oldvalues,
        ['bio']: ''
      }));
      setBioFormStatus((oldvalues) => ({ ...oldvalues, ['bio']: false }));
    }
  };
  const handleSubmitBio = async (event) => {
    let ar = [];
    bioForm.tags.map((value, index) => {
      ar.push(value.value);
    });

    const data = JSON.stringify({
      tags: ar,
      bio: bioForm.bio
    });

    try {
      const response = await fetch('http://localhost:5000/settings/tags', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json'
        },
        body: data,
        credentials: 'include'
      });
      const body = await response.json();
      if (response.status !== 200) {
        throw new error(body.merror);
      }
      setMerror('successChangeSettings');
    } catch (e) {
      setMerror('failChangeSettings');
    }
  };

  return (
    <React.Fragment>
      <div id="merrorSettings">{merror}</div>
      <Grid item xl={8} xs={12}>
        <FormControl
          className="settingsInputForm settingsInputFormTags"
          error={bioFormStatus.bio !== true}
        >
          <InputLabel htmlFor="bioInput" className="labelInputForm">
            Bio
          </InputLabel>
          <Input
            defaultValue={bioForm.bio}
            id="bioSettings"
            type="text"
            onBlur={handleChangeBio}
            className="settingsInput"
          />
        </FormControl>
      </Grid>
      <Grid item xl={8} xs={12}>
        <Select
          label="Tags"
          className="firstInput labelTagsSettings"
          closeMenuOnSelect={false}
          components={animatedComponents}
          onChange={handleChangeTags}
          defaultValue={bioForm.tags}
          isMulti
          options={options}
        />
      </Grid>
      <div id="buttonSettingsPrivate">
        <Button
          variant="outlined"
          id="tagsButtonSettings"
          onClick={handleSubmitBio}
        >
          Confirm
        </Button>
      </div>
    </React.Fragment>
  );
};
SettingsTags.getInitialProps = async (ctx) => { };

export default SettingsTags;
