import React from 'react';
import PropTypes from 'prop-types';
import { Button } from '@material-ui/core';

const CookieIndicator = ({ t, setAnchorIndicator }) => {
  return (
    <React.Fragment>
      <div id="alertCookieContainer">
        <p id="paragrapheCookie">
          En poursuivant votre navigation sur ce site, vous acceptez
          l'utilisation de cookies
        </p>
        <Button
          onClick={setAnchorIndicator}
          variant="contained"
          id="buttonCookie"
        >
          Ok
        </Button>
      </div>
    </React.Fragment>
  );
};

export default CookieIndicator;
