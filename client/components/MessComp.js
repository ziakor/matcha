import React, { useState } from 'react';
import PropTypes from 'prop-types';
import Typography from '@material-ui/core/Typography';
import FormControl from '@material-ui/core/FormControl';
import Button from '@material-ui/core/Button';
import SendIcon from '@material-ui/icons/Send';
import Cookies from 'universal-cookie';
import io from 'socket.io-client';

const cookies = new Cookies();
const upperCase = (string) => {
  if (string) return string.charAt(0).toUpperCase() + string.slice(1);
};

const MessComp = ({ myData, userData }) => {
  const [currentConv, setCurrentConv] = useState(null);

  const socket = io.connect('http://localhost:5001', {
    autoConnect: true,
    transports: ['websocket'],
    query: 'tokenCookie=' + cookies.get('tokenCookie')
  });

  const [message, setMessage] = useState({
    myMessage: [],
    hisMessage: []
  });
  const [newMessage, SetNewMessage] = useState('Your text');
  const handleSubmitMessage = async (event) => {
    if (currentConv) {
      const data = new FormData();
      data.append('message', newMessage);
      data.append('pseudo', currentConv.name);
      try {
        const response = await fetch('http://localhost:5000/chat/addMessage', {
          method: 'POST',
          credentials: 'include',
          body: data
        });
        if (response.status === 200) {
          //AJOUTER LES MESSAGES

          setListMess((oldvalues) => {
            let newtab = oldvalues;
            newtab.push(newMessage);
            return newtab;
          });
          setTypeMess((oldvalues) => {
            let newtab = oldvalues;
            newtab.push(0);
            return newtab;
          });

          await SetNewMessage('');
        }
      } catch (e) {
      }
    }
  };

  const sortMessage = async (message) => {
    let sortTab = [];
    for (let i = 0; i <= message.myMessage.length - 2; i += 2) {
      sortTab.push({
        message: message.myMessage[i],
        time: parseInt(message.myMessage[i + 1]),
        type: 0
      });
    }
    for (let j = 0; j <= message.hisMessage.length - 2; j += 2) {
      sortTab.push({
        message: message.hisMessage[j],
        time: parseInt(message.hisMessage[j + 1]),
        type: 1
      });
    }
    let k = 0;
    while (k < sortTab.length - 1) {
      if (sortTab[k].time > sortTab[k + 1].time) {
        let tmp = sortTab[k];
        sortTab[k] = sortTab[k + 1];
        sortTab[k + 1] = tmp;
        k = 0;
      } else k++;
    }
    return sortTab;
  };


  const handleChangeCurrentElem = (elem) => async (event) => {
    if (elem) {


      const data = new FormData();
      data.append('pseudo', elem.name);

      try {
        const response = await fetch(
          `http://localhost:5000/chat/getRoom/${elem.name}`,
          {
            method: 'GET',
            headers: {
              authorization: cookies.get('tokenCookie')
            }
          }
        );
        if (response.status === 200) {
          const body = await response.json();
          await setCurrentConv({
            name: body.name,
            connected: body.name,
            image: `/static/dbImages/${body.image}`
          });
          await setMessage({
            myMessage: body.myMessage,
            hisMessage: body.hisMessage
          });
          const tabsort = await sortMessage(message);
          //FILL LISTMESSAGE

          await setListMess(() => {
            let tab = [];
            let i = 0;
            while (i < tabsort.length) {
              tab.push(tabsort[i].message);
              i++;
            }
            return tab;
          });

          await setTypeMess(() => {
            let tab = [];
            let i = 0;
            while (i < tabsort.length) {
              tab.push(tabsort[i].type);
              i++;
            }
            return tab;
          });
          await SetNewMessage(' ');
          await SetNewMessage('');
          await SetNewMessage(' ');
        }
      } catch (e) {
      }


      setCurrentConv(elem);
    }
  };
  const ConvListElem = ({ elem }) => (
    <div className="convListElems" onClick={handleChangeCurrentElem(elem)}>
      <div>
        <img alt="myAvatar" className="avatarDiv2" src={elem.image} />
      </div>
    </div>
  );
  const CurrentElemConv = () => (
    <div id="divCurrent">
      <div>
        <img alt="myAvatar" id="avatarDivCurrent" src={currentConv.image} />
      </div>
      <Typography id="discussTextCurrent" align="center" variant="h5">
        {currentConv.name}
      </Typography>
      <Typography id="DiscussConnected" variant="body2">
        En ligne il y a {currentConv.connected}
      </Typography>
    </div>
  );

  const handleChangeMessage = (event) => {
    SetNewMessage(event.target.value);
  };
  const [listMess, setListMess] = useState([]);
  const [typeMess, setTypeMess] = useState([]);

  socket.on('message', async (data) => {
    if (data !== undefined && currentConv !== null) {
      if (data.pseudo === currentConv.name) {
        await setListMess((oldvalues) => {
          let tab = oldvalues;
          oldvalues.push(data.message);
          return oldvalues;
        });
        await setTypeMess((oldvalues) => {
          let tab = oldvalues;
          oldvalues.push(1);
          return oldvalues;
        });
        await SetNewMessage(' ');
        await SetNewMessage('');
      }
    }
  });

  return (
    <React.Fragment>
      <div id="messContainer">
        <div id="firstLine">
          <div id="myself">
            <div>
              <img alt="myAvatar" id="avatarDiv" src={myData.images[0]} />
            </div>
            {/* <Typography id="discussText" align="center" variant="h5">Discussions</Typography> */}
          </div>
          <div id="currentConvGuy">
            {currentConv !== null && <CurrentElemConv />}
          </div>
        </div>
        <div id="secondLine">
          <div id="convList">
            {userData.map((i, index) => (
              <ConvListElem elem={i} key={`${i.name}-${index}-${i.image}`} />
            ))}
          </div>
          <div id="currentConv">
            <div id="conversation">
              {listMess.map((value, index) => {
                return (
                  <div
                    className={
                      typeMess[index] === 1 ? 'yours messages' : 'mine messages'
                    }
                    key={`${index}-${value}`}
                  >
                    <div className="message last">{value}</div>
                  </div>
                );
              })}
            </div>
            {currentConv && (
              <div id="messageInputDiv">
                <div id="sendMessage">
                  <input
                    id="messageInput"
                    type="text"
                    value={newMessage}
                    onChange={handleChangeMessage}
                    autoComplete="off"
                    placeholder={"asdn"}
                    required={true}

                  />
                </div>
                <Button id="blueConv" onClick={handleSubmitMessage}>
                  <SendIcon />
                </Button>
              </div>
            )}
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

MessComp.getInitialProps = async () => { };

export default MessComp;
