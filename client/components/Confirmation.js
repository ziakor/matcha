import React, { useEffect, useState } from 'react';

import * as Qs from 'query-string';

const Confirmation = ({ t, salsifi }) => {
  const [toogle, setToogle] = useState({
    status: false,
    class: ''
  });
  const [mount, setMount] = useState(false);
  //A REFAIRE POUR PASSER CA EN SSR
  useEffect(() => {
    if (mount === false) {
      const parsed = Qs.parse(location.search);
      if (typeof parsed.res === 'string') {
        if (parsed.res === 'true') {
          setToogle({ status: true, class: 'successConfirmation' });
          setTimeout(() => {
            setToogle({ status: false, class: '' });
          }, 5000);
        } else if (parsed.res === 'false') {
          setToogle({ status: true, class: 'failConfirmation' });
          setTimeout(() => {
            setToogle({ status: false, class: '' });
          }, 5000);
        }
      }
      setMount(true);
    }
  }, [toogle]);
  return (
    <React.Fragment>
      {toogle.status === true ? (
        <div id={toogle.class}>{toogle.class}</div>
      ) : null}
    </React.Fragment>
  );
};

export default Confirmation;
