import React, { useState } from 'react';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import Swipeable from 'react-swipy';
import FavoriteIcon from '@material-ui/icons/Favorite';
import ClearRoundedIcon from '@material-ui/icons/ClearRounded';
import Toolbar from '@material-ui/core/Toolbar';
import MenuFilter from './MenuFilter';
import Cookies from 'universal-cookie';
import Router from 'next/router';
import Link from 'next/link';

const cookies = new Cookies();

const MatchSwipe = ({ userData, hobbies }) => {
  const [myHobbies, setMyHobbies] = useState(hobbies);
  const [suggest, setSuggest] = useState(userData);
  const [messageLike, setMessageLike] = useState('');
  const upperCase = (string) => {
    if (string) return string.charAt(0).toUpperCase() + string.slice(1);
  };
  const [cards, setCards] = React.useState(() => {
    let tmp = [];
    for (let i of suggest)
      tmp.push([
        upperCase(i.firstName) + ',',
        i.age,
        i.name,
        i.geo[2],
        'url(' + i.images[0] + ')'
      ]);
    return tmp;
  });

  const toProfil = () => {
    Router.push('/profil/' + cards[0][2]);
  };
  const Card = ({ zIndex = 0, children, ImageV }) => (
    <Link href={'profil?pseudo=' + cards[0][2]} as={'/profil/' + cards[0][2]}>
      <div className="containerCard">
        <div
          className="cardStyle"
          style={{
            backgroundImage: ImageV,
            zIndex
          }}
        >
          {children}
        </div>
        <div id="descriptionPeople">{cards[0][0] + ' ' + cards[0][1]}</div>
      </div>
    </Link>
  );
  const Card2 = ({ zIndex = 0, children, ImageV }) => (
    <div className="containerCard">
      <div
        className="cardStyle2"
        style={{
          backgroundImage: ImageV,
          zIndex
        }}
      ></div>
      <div id="descriptionPeople2">{cards[1][0] + ' ' + cards[1][1]}</div>
    </div>
  );
  const Card3 = ({ zIndex = 0, children }) => (
    <div className="containerCard">
      <div className="cardStyle3">{children}</div>
    </div>
  );
  const remove = () => {
    setCards(cards.slice(1));
  };

  const handleLeftClick = () => {
    console.log('Vous avez viré' + cards[0][0]);
  };
  const handleRightClick = async () => {
    try {
      const response = await fetch(
        'http://localhost:5000/profil/like/' + cards[0][2],
        {
          method: 'GET',
          headers: {
            credentials: 'include',
            authorization: cookies.get('tokenCookie')
          }
        }
      );
      if (response.status !== 200) {
        console.log('failLike');
      } else console.log('vous avez like ', cards[0][0]);
    } catch (error) {

    }
  };

  return (
    <React.Fragment>
      <div id="appStyle">
        <div className="deckStyles">
          {cards.length > 0 && (
            <div id="wrapperStyle">
              <Swipeable
                buttons={({ left, right }) => (
                  <div id="actionsStyle">
                    <Button id="red" onClick={left}>
                      <ClearRoundedIcon fontSize="large" />
                    </Button>
                    <Button id="blue" onClick={right}>
                      <FavoriteIcon fontSize="large" />
                    </Button>
                    <Toolbar id="ToolFilter">
                      <MenuFilter
                        suggest={suggest}
                        setSuggest={setSuggest}
                        setCards={setCards}
                        myHobbies={myHobbies}
                      />
                    </Toolbar>
                  </div>
                )}
                onAfterSwipe={remove}
                onSwipe={(direction) => {
                  if (direction === 'left') handleLeftClick();
                  else handleRightClick();
                }}
              >
                <Card ImageV={cards[0][4]}></Card>
              </Swipeable>
              {cards.length > 1 && (
                <Card2 zIndex={-1} ImageV={cards[1][4]}></Card2>
              )}
            </div>
          )}
          {cards.length < 1 && (
            <div id="wrapperStyle">
              <Card3 zIndex={-1}>There is no matching profils</Card3>
            </div>
          )}
        </div>
      </div>
    </React.Fragment>
  );
};

export default MatchSwipe;
