import React from 'react';
import {
  CarouselProvider,
  Slider,
  Slide,
  ButtonBack,
  ButtonNext
} from 'pure-react-carousel';
import 'pure-react-carousel/dist/react-carousel.es.css';

const CarouselBackground = () => (
  <div>
    <CarouselProvider
      naturalSlideWidth={2560}
      naturalSlideHeight={1440}
      totalSlides={2}
      visibleSlides={1}
      isPlaying={true}
      interval={3000}
      infinite={'true'}
    >
      <Slider className="CarouselBackgroundSlider">
        <Slide index={0}>
          <div>
            <img
              src="/static/images/image1.jpg"
              alt="background"
              className="image1"
            />
          </div>
        </Slide>
        <Slide index={1}>
          <div>
            <img
              src="/static/images/image2.jpg"
              alt="aawdawdawdwd"
              className="image2"
            />
          </div>
        </Slide>
      </Slider>
    </CarouselProvider>
  </div>
);

export default CarouselBackground;
