import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Router, useRouter } from 'next/router';
import Avatar from '@material-ui/core/Avatar';
import Grid from '@material-ui/core/Grid';
import Divider from '@material-ui/core/Divider';
import Button from '@material-ui/core/Button';
import FavoriteIcon from '@material-ui/icons/Favorite';
import FavoriteBorderIcon from '@material-ui/icons/FavoriteBorder';
import ReportProblemIcon from '@material-ui/icons/ReportProblem';
import BlockIcon from '@material-ui/icons/Block';
import Modal from '@material-ui/core/Modal';
import Tooltip from '@material-ui/core/Tooltip';
import Cookies from 'universal-cookie';
import Brightness1Icon from '@material-ui/icons/Brightness1';
import {
  CarouselProvider,
  Slider,
  Slide,
  ButtonBack,
  ButtonNext
} from 'pure-react-carousel';
import 'pure-react-carousel/dist/react-carousel.es.css';
const cookies = new Cookies();

const ProfileContainer = ({ userData }) => {
  const router = useRouter();
  const [openReport, setOpenReport] = useState(false);
  const [messageReport, setMessageReport] = useState('');

  const [openBlock, setOpenBlock] = useState(false);
  const [messageBlock, setMessageBlock] = useState('');

  const [messageLike, setMessageLike] = useState('');
  const [statusLike, setStatusLike] = useState(userData.like);

  let finalImages = [];
  for (let j = 0; j < userData.images.length; j++) {
    if (userData.images[j] !== '' && userData.images[j] !== undefined) {
      finalImages.push(userData.images[j]);
    }
    // else finalImages.push("https://acegif.com/wp-content/gifs/fire-102.gif");
  }
  userData.images = finalImages;
  const MultipleImage = () => {
    if (userData.images.length > 1) {
      return (
        <CarouselProvider
          naturalSlideWidth={2560}
          className="myprofileCarousel"
          naturalSlideHeight={1440}
          totalSlides={userData.images.length}
          visibleSlides={1}
          infinite={'true'}
        >
          <Slider>
            {userData.images.map((value, index) => {
              return (
                <Slide index={index} key={`${value}-${index}`}>
                  <div>
                    <img src={value} alt="user" className="image3" />
                  </div>
                </Slide>
              );
            })}
          </Slider>
        </CarouselProvider>
      );
    } else
      return (
        <CarouselProvider
          naturalSlideWidth={2560}
          className="myprofileCarousel"
          naturalSlideHeight={1440}
          totalSlides={userData.images.length}
          visibleSlides={1}
          infinite={'true'}
        >
          <Slider>
            {userData.images.map((value, index) => {
              return (
                <Slide index={index} key={`${value}-${index}`}>
                  <div>
                    <img src={value} alt="user" className="image3" />
                  </div>
                </Slide>
              );
            })}
          </Slider>
        </CarouselProvider>
      );
  };

  const showLike = () => {
    if (statusLike === 0)
      return <FavoriteBorderIcon id="likeIcon" className="noLike" />;
    else if (statusLike === 1)
      return <FavoriteBorderIcon id="likeIcon" className="iLike" />;
    else if (statusLike === 2) {
      return <FavoriteIcon id="likeIcon" className="shelikeAndNotMe" />;
    } else {
      return <FavoriteIcon id="likeIcon" className="twoLike" />;
    }
  };

  const handleChangeReport = () => {
    setOpenReport((oldvalue) => {
      return !oldvalue;
    });
  };

  const handleSubmitReport = async () => {
    try {
      const response = await fetch(
        `http://localhost:5000/profil/report/${router.query.pseudo}`,
        {
          method: 'GET',
          headers: {
            credentials: 'include',
            authorization: cookies.get('tokenCookie')
          }
        }
      );

      if (response.status !== 200) setMessageReport('failReport');
      else {
        setMessageReport('successReport');
      }
    } catch (e) {
      setMessageReport('failReport');
    }
  };

  const handleChangeBlock = () => {
    setOpenBlock((oldvalue) => {
      return !oldvalue;
    });
  };

  const handleSubmitBlock = async () => {
    try {
      const response = await fetch(
        `http://localhost:5000/profil/block/${router.query.pseudo}`,
        {
          method: 'GET',
          headers: {
            credentials: 'include',
            authorization: cookies.get('tokenCookie')
          }
        }
      );
      if (response.status !== 200) setMessageBlock('failBlock');
      else setMessageBlock('successBlock');
    } catch (e) {
      setMessageBlock('failBlock');
    }
  };

  const handleChangeLike = async () => {
    const data = new FormData();
    data.append('like', like);
    try {

      const response = await fetch(
        `http://localhost:5000/profil/like/${router.query.pseudo}`,
        {
          method: 'GET',
          headers: {
            credentials: 'include',
            authorization: cookies.get('tokenCookie')
          }
        }
      );
      if (response.status !== 200) setMessageLike('failLike');
      else {
        const body = await response.json();
        if (body.ret === 0) {
          setStatusLike((oldvalue) => {
            return oldvalue - 1;
          });
        } else {
          setStatusLike((oldvalue) => {
            return oldvalue + 1;
          });
        }
        setMessageLike('successLike');
      }
    } catch (e) {
      setMessageLike('failLike');
    }
  };
  const displaySexualOrientation = () => {
    if (userData.genre === "H") {
      if (userData.target === "H")
        return "Woman"
      else if (userData.target === "F")
        return "Man"
      else
        return "Both"
    }
    else {
      if (userData.target === "H")
        return "Man"
      else if (userData.target === "F")
        return "Woman"
      else
        return "Both"
    }
  }
  const like = showLike();
  return (
    <Grid item xs={12} md={10} xl={8} id="myProfileContainer">
      <Grid item id="infosMyProfile2" xs={8} md={6} xl={4}>
        <button title="Report" id="reportIcon" onClick={handleChangeReport}>
          <ReportProblemIcon />
        </button>
        <Modal
          aria-labelledby="modal-report-fake-account"
          aria-describedby="modal report fake account"
          open={openReport}
          onClose={handleChangeReport}
        >
          <div id="reportContainer">
            <h1 id="h1Report">Report user</h1>
            Report this user as a fake account ?
            <Divider id="dividerReport" />
            <div id="buttonReport">
              <Button
                variant="contained"
                color="secondary"
                id="buttonNoReport"
                onClick={handleChangeReport}
              >
                No
              </Button>
              <Button
                variant="contained"
                color="primary"
                id="buttonYesReport"
                onClick={handleSubmitReport}
              >
                Yes
              </Button>
            </div>
          </div>
        </Modal>

        <button title="Block user" id="blockIcon" onClick={handleChangeBlock}>
          <BlockIcon />
        </button>

        <Modal
          aria-labelledby="modal-block-account"
          aria-describedby="modal-report-block-account"
          open={openBlock}
          onClose={handleChangeBlock}
        >
          <div id="blockContainer">
            <h1 id="h1Report2">Block user</h1>
            Really wanna block this user ?
            <Divider id="dividerBlock" />
            <div id="buttonReport">
              <Button
                variant="contained"
                color="secondary"
                id="buttonNoReport"
                onClick={handleChangeBlock}
              >
                No
              </Button>
              <Button
                variant="contained"
                color="primary"
                id="buttonYesReport"
                onClick={handleSubmitBlock}
              >
                Yes
              </Button>
            </div>
          </div>
        </Modal>

        <Grid item xl={12}>
          <div id="avatarContainerProfile">
            {userData.images[0] !== '' && (
              <Avatar
                alt={'user'}
                sizes="80px"
                id="avatarProfile2"
                src={`${userData.images[0]}`}
              />
            )}
          </div>
          {/*//*/}

          <button id="likeIcon" onClick={handleChangeLike}>
            {like}
          </button>
          <Tooltip
            title={userData.connected === '' ? 'Available' : userData.connected}
          >
            {userData.connected === '' ? (
              <Brightness1Icon id="connectedTrue" />
            ) : (
                <Brightness1Icon id="connectedFalse" />
              )}
          </Tooltip>
        </Grid>
        <Grid item xl={12} id="pseudoContainer">
          <div id="tipsPseudo">Pseudo:</div>
          <div id="pseudoContainerProfile">
            <h1 className="h1Profile">{userData.name}</h1>
          </div>
          <Divider />
        </Grid>
        <Grid item xl={12} id="pseudoContainer">
          <div id="tipsPseudo">Age:</div>
          <div id="pseudoContainerProfile">
            <h1 className="h1Profile">{parseFloat(userData.age)}</h1>
          </div>
          <Divider />
        </Grid>
        <Grid item xl={12}>
          <div id="nameContainerProfile">
            <h1 className="h1Profile">
              {userData.firstName} {userData.lastName}
            </h1>
          </div>
        </Grid>
        <Grid item xl={12} id="genreContainer">
          <div id="tipsGenre">Gender:</div>
          <div id="genderContainerProfile">
            <h1 className="h1Profile">{userData.genre}</h1>
          </div>
          <Divider />
        </Grid>
        <Grid item xl={12} id="geoContainer">
          <div id="tipsGeo">Town:</div>
          <div id="cityContainerProfile">
            <h1 className="h1Profile">{userData.geo[2]}</h1>
          </div>
          <Divider />
        </Grid>

        <Grid item xl={12} id="targetContainer">
          <div id="tipsTarget">Interested by:</div>
          <div id="sexualContainerProfile">
            <h1 className="h1Profile h1Target">{displaySexualOrientation()}</h1>
          </div>
          <div></div>
          <Divider />
        </Grid>
        <Grid item xl={12} id="targetContainer">
          <div id="tipsTarget">Reputation:</div>
          <div id="sexualContainerProfile">
            <h1 className="h1Profile h1Target">{userData.reputation}</h1>
          </div>
          <div></div>
          <Divider />
        </Grid>
        <Divider className="dividerProfil" />
        <Grid item xl={12} id="bioContainer">
          <h2 id="titleBio">Bio:</h2>
          <div id="bioContainerProfile">
            <span id="bioSpan">{userData.bio}</span>
          </div>
          <div></div>
        </Grid>
        <Divider className="dividerProfil" />
        <Grid item xl={12} id="tagsContainer">
          <div id="tagsContainerProfile">
            <h3 id="titleTags">Tags:</h3>
            {/*<span id="tagsSpan">{userData.tags.join(', ')}</span>*/}
            <div id="tagsCont">
              {userData.tags.map((value, index) => {
                if (index < userData.tags.length - 1) {
                  value = `${value}, `;
                }
                return (
                  <span key={`${value}${index}`}>
                    <span className="hash">#</span>
                    {value}
                  </span>
                );
              })}
            </div>
          </div>
          <div></div>
        </Grid>
      </Grid>
      <MultipleImage />
    </Grid>
  );
};

ProfileContainer.getInitialProps = async (ctx) => { };
ProfileContainer.propTypes = {
  userData: PropTypes.object.isRequired
};

export default ProfileContainer;
