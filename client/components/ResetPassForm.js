import React from 'react';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Link from 'next/link';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import Router from 'next/router';
import {
  FormControl,
  InputLabel,
  OutlinedInput,
  Input,
  ButtonGroup
} from '@material-ui/core';

const ResetPassForm = () => {
  const [pseudoMailCheck, setPseudoMailCheck] = React.useState(true);
  const [pseudoMail, setPseudoMail] = React.useState('');
  const [message, setMessage] = React.useState('');

  const handleCheckMail = (event) => {
    const regexmail = /\S+@\S+\.\S+$/;
    if (regexmail.test(event.target.value)) {
      setPseudoMailCheck(true);
      setPseudoMail(event.target.value);
    } else {
      setPseudoMailCheck(false);
    }
  };

  const [isLogguedIn, SetIsLogguedIn] = React.useState('null');
  const submitMotherFucker = async () => {
    if (!pseudoMailCheck) {
      return 'Invalid Email';
    }
    const data = JSON.stringify({
      mail: pseudoMail
    });
    try {
      const response = await fetch('http://localhost:5000/user/reset', {
        method: 'POST',
        body: data,
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json'
        }
      });
      const body = await response.json();
      if (response.status === 200) {
        SetIsLogguedIn(true);
        setTimeout(() => {
          Router.push('/signin');
        }, 2900);
      } else {
        SetIsLogguedIn(false);
      }
      // return body;
    } catch (error) {
      return [].push(error);
    }
  };

  return (
    <React.Fragment>
      <div id="ResetFormContainer">
        <div id="connexionWordForm">
          <Typography id="connexionWordReset" align="center" variant="inherit">
            Reset password
          </Typography>
        </div>
        <form id="signinFormReset">
          <Grid container item xs={12} justify="center" alignItems="center">
            <Typography id="resetExplain" align="center" variant="inherit">
              Enter your email address and we will send you a new password.
            </Typography>
            <FormControl
              className="signinInputForm"
              required
              error={pseudoMailCheck === false}
            >
              <InputLabel id="InputLabelConnexion">Email</InputLabel>
              <Input
                id="pseudomailInput"
                autoComplete="off"
                type="text"
                onBlur={handleCheckMail}
              />
            </FormControl>
          </Grid>
          <Button
            variant="contained"
            id="buttonConnexionReset"
            className="buttonHomeReset"
            onClick={submitMotherFucker}
          >
            Reset
          </Button>
          {isLogguedIn === true && (
            <h1 id="mailSend">An Email has been sent with your new password</h1>
          )}
        </form>
      </div>
    </React.Fragment>
  );
};

ResetPassForm.getInitialProps = async () => { };

export default ResetPassForm;
