import React, { useState } from 'react';
import PropTypes from 'prop-types';
import Tabs from '@material-ui/core/Tabs';
import Grid from '@material-ui/core/Grid';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import fetch from 'isomorphic-unfetch';
import Cookies from 'universal-cookie';
import ModalImage from 'react-modal-image';
import SaveIcon from '@material-ui/icons/Save';
import dynamic from 'next/dynamic';
import PublishOutlinedIcon from '@material-ui/icons/PublishOutlined';
import AddCircleOutlineOutlinedIcon from '@material-ui/icons/AddCircleOutlineOutlined';
import DeleteForeverOutlinedIcon from '@material-ui/icons/DeleteForeverOutlined';
import {
  FormControl,
  InputLabel,
  FormGroup,
  FormLabel,
  OutlinedInput,
  FormControlLabel,
  Input,
  Tooltip,
  FormHelperText,
  Switch,
  Select
} from '@material-ui/core';
import Router from 'next/router';

const SettingsTags = dynamic(() => import('./SettingsTags.js'), {
  loading: () => null,
  ssr: false
});

const cookies = new Cookies();

const SettingsForm = ({ userData, userTags }) => {
  const [activeTab, setActiveTab] = useState(0);
  const [merror, setMerror] = useState('');
  const [data, setData] = useState(userData[0] !== '');
  if (data === false) {
    setTimeout(() => {
      setData(true);
    }, 1000);
  }

  //STATE PRIVATE
  const [privateFormStatus, setPrivateFormStatus] = useState({
    mail: true,
    password: true,
    confirm: true,
    geo: false
  });
  const [privateForm, setPrivateForm] = useState({
    mail: '',
    password: '',
    confirm: '',
    geo: [0, 0]
  });

  //STATE PUBLIC
  const [publicForm, setPublicForm] = useState({
    pseudo: '',
    firstName: '',
    lastName: '',
    gender: '',
    sexualOrientation: ''
  });
  const [publicFormStatus, setPublicFormStatus] = useState({
    pseudo: true,
    firstName: true,
    lastName: true,
    gender: true,
    sexualOrientation: true
  });

  //STATE IMAGE
  const [images, setImages] = useState({
    image0: userData[0],
    image1: userData[1],
    image2: userData[2],
    image3: userData[3],
    image4: userData[4]
  });
  const [imageData, setImageData] = useState({
    image0: null,
    image1: null,
    image2: null,
    image3: null,
    image4: null
  });
  //STATE BIO AND TAGS
  const [bioForm, setBioForm] = useState({
    bio: '',
    tags: []
  });

  //GENERAL FUNCTIONS

  const TabPanel = (props) => {
    const { value, index, children, ...other } = props;

    return (
      <Typography
        component="div"
        role="tabpanel"
        hidden={value !== index}
        id={`vertical-tabpanel-${index}`}
        aria-labelledby={`vertical-tab-${index}`}
        className={
          activeTab !== index
            ? 'displayHidden contentTabSettings'
            : 'contentTabSettings'
        }
        {...other}
      >
        <Box p={3}>{children}</Box>
      </Typography>
    );
  };

  const handleChangeTab = (event, newValue) => {
    setActiveTab(newValue);
    setMerror('');
  };

  //PRIVATE FUNCTIONS
  const handleChangePassword = (event) => {
    event.preventDefault();
    const str = event.target.value;
    const regex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/;
    if (regex.test(str)) {
      setPrivateForm((oldvalue) => ({ ...oldvalue, ['password']: str }));
      setPrivateFormStatus((oldvalue) => ({ ...oldvalue, ['password']: true }));
    } else {
      setPrivateForm((oldvalue) => ({ ...oldvalue, ['password']: '' }));
      setPrivateFormStatus((oldvalue) => ({
        ...oldvalue,
        ['password']: false
      }));
    }
  };
  const handleChangeConfirmPassword = (event) => {
    event.preventDefault();
    const str = event.target.value;
    const regex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/;
    if (regex.test(str) && str === privateForm.password) {
      setPrivateForm((oldvalue) => ({ ...oldvalue, ['confirm']: str }));
      setPrivateFormStatus((oldvalue) => ({ ...oldvalue, ['confirm']: true }));
    } else {
      setPrivateForm((oldvalue) => ({ ...oldvalue, ['confirm']: '' }));
      setPrivateFormStatus((oldvalue) => ({
        ...oldvalue,
        ['confirm']: false
      }));
    }
  };
  const handleChangeMail = (event) => {
    event.preventDefault();
    const str = event.target.value;
    const regex = /\S+@\S+\.\S+$/;
    if (regex.test(str)) {
      setPrivateForm((oldvalues) => ({ ...oldvalues, ['mail']: str }));
      setPrivateFormStatus((oldvalues) => ({ ...oldvalues, ['mail']: true }));
    } else {
      setPrivateForm((oldvalues) => ({ ...oldvalues, ['mail']: '' }));
      setPrivateFormStatus((oldvalues) => ({ ...oldvalues, ['mail']: false }));
    }
  };

  const handleChangeGeo = async (event) => {
    if (!privateFormStatus.geo === true) {
      navigator.geolocation.getCurrentPosition(
        (position) => {
          setPrivateForm((oldvalues) => ({
            ...oldvalues,
            ['geo']: [position.coords.latitude, position.coords.longitude]
          }));
          setPrivateFormStatus((oldvalues) => ({
            ...oldvalues,
            ['geo']: true
          }));
        },
        null,
        { enableHighAccuracy: true, maximumAge: 0 }
      );
    } else {
      setPrivateForm((oldvalues) => ({
        ...oldvalues,
        ['geo']: [0, 0]
      }));
      setPrivateFormStatus((oldvalues) => ({
        ...oldvalues,
        ['geo']: false
      }));
    }
  };

  const handleSubmitPrivate = async (event) => {
    event.preventDefault();
    let data = new FormData();
    data.append('mail', privateForm.mail);
    data.append('password', privateForm.password);
    data.append('geo', privateForm.geo[0]);
    data.append('geo', privateForm.geo[1]);

    try {
      const response = await fetch('http://localhost:5000/settings/private', {
        method: 'POST',
        body: data,
        credentials: 'include'
      });
      const body = await response.json();
      if (response.status !== 200) {
        throw new Error(body.merror);
      }

      setMerror('successChangeSettings');
    } catch (e) {
      setMerror('failChangeSettings');
    }
  };

  const disableButtonPrivate = (event) => {
    return (
      (privateForm.mail === '' &&
        privateForm.password === '' &&
        privateForm.confirm === '' &&
        privateForm.geo[0] === 0) ||
      privateFormStatus.confirm === false ||
      privateFormStatus.password === false ||
      privateFormStatus.mail === false ||
      privateForm.geo === false
    );
  };

  const handleResetPassword = async (event) => {
    try {
      const response = await fetch(
        'http://localhost:5000/settings/resetPassword',
        {
          credentials: 'include',
          headers: {
            authorization: cookies.get('tokenCookie')
          }
        }
      );
      if (response.status !== 200) setMerror('errorResetPassword');
      else {
        cookies.remove('tokenCookie');
        await Router.push('/');
        setMerror('successResetPassword');
      }
    } catch (e) {
      setMerror('errorResetPassword');
    }
  };
  //PUBLIC FUNCTIONS

  const handleChangePseudo = (event) => {
    event.persist();

    const regexpseudo = /^[a-zA-Z]{4,15}$/;

    if (regexpseudo.test(event.target.value)) {
      setPublicForm((oldvalues) => ({
        ...oldvalues,
        ['pseudo']: event.target.value
      }));
      setPublicFormStatus((oldvalues) => ({ ...oldvalues, ['pseudo']: true }));
    } else {
      setPublicForm((oldvalues) => ({
        ...oldvalues,
        ['pseudo']: ''
      }));
      cookies.remove('tokenCookie');
      Router.push('/');

      setPublicFormStatus((oldvalues) => ({ ...oldvalues, ['pseudo']: false }));
    }
  };
  const handleChangeFirstName = (event) => {
    if (event.target.value !== '') {
      setPublicForm((oldvalues) => ({
        ...oldvalues,
        ['firstName']: event.target.value
      }));
      setPublicFormStatus((oldvalues) => ({
        ...oldvalues,
        ['firstName']: true
      }));
    } else {
      setPublicForm((oldvalues) => ({
        ...oldvalues,
        ['firstName']: ''
      }));
      setPublicFormStatus((oldvalues) => ({
        ...oldvalues,
        ['firstName']: false
      }));
    }
  };

  const handleChangeLastName = (event) => {
    if (event.target.value !== '') {
      setPublicForm((oldvalues) => ({
        ...oldvalues,
        ['lastName']: event.target.value
      }));
      setPublicFormStatus((oldvalues) => ({
        ...oldvalues,
        ['lastName']: true
      }));
    } else {
      setPublicForm((oldvalues) => ({
        ...oldvalues,
        ['lastName']: ''
      }));
      setPublicFormStatus((oldvalues) => ({
        ...oldvalues,
        ['lastName']: false
      }));
    }
  };

  const handleChangeSexualorientation = (event) => {
    event.persist();
    if (
      event.target.value === 'H' ||
      event.target.value === 'F' ||
      event.target.value === 'B'
    ) {
      setPublicForm((oldvalues) => ({
        ...oldvalues,
        ['sexualOrientation']: event.target.value
      }));
      setPublicFormStatus((oldvalues) => ({
        ...oldvalues,
        ['sexualOrientation']: true
      }));
    } else {
      setPublicForm((oldvalues) => ({
        ...oldvalues,
        ['sexualOrientation']: ''
      }));
    }
  };

  const handleChangeGenre = (event) => {
    event.persist();
    if (event.target.value === 'H' || event.target.value === 'F') {
      setPublicFormStatus((oldvalues) => ({
        ...oldvalues,
        ['gender']: true
      }));
      setPublicForm((oldvalues) => ({
        ...oldvalues,
        ['gender']: event.target.value
      }));
    } else {
      setPublicFormStatus((oldvalues) => ({
        ...oldvalues,
        ['gender']: false
      }));
      setPublicForm((oldvalues) => ({
        ...oldvalues,
        ['gender']: ''
      }));
    }
  };

  const handleSubmitPublic = async (event) => {
    event.preventDefault();

    const data = new FormData();
    data.append('pseudo', publicForm.pseudo);
    data.append('firstName', publicForm.firstName);
    data.append('lastName', publicForm.lastName);
    data.append('gender', publicForm.gender);
    data.append('sexualOrientation', publicForm.sexualOrientation);
    try {
      const response = await fetch('http://localhost:5000/settings/public', {
        method: 'POST',
        body: data,
        credentials: 'include'
      });
      const body = await response.json();

      if (response.status !== 200) {
        throw new Error(body.merror);
      }
      setMerror('successChangeSettings');
    } catch (e) {
      setMerror('failChangeSettings');
    }
  };

  const disableButtonPublic = (event) => {
    if (
      (publicForm.gender === '' &&
        publicForm.sexualOrientation === '' &&
        publicForm.firstName === '' &&
        publicForm.lastName === '' &&
        publicForm.pseudo === '') ||
      (publicFormStatus.sexualOrientation === false ||
        publicFormStatus.gender === false ||
        publicFormStatus.pseudo === false) ||
      publicFormStatus.firstName === false ||
      publicFormStatus.lastName === false
    ) {
      return true;
    }
    return false;
  };

  //IMAGES FUNCTIONS

  const handleChangeImage = (id) => (event) => {
    if (id === -1) {
      id = getNumberImage();
    }
    const error = [];
    const file = event.target.files[0];
    const reader = new FileReader();
    const types = ['image/png', 'image/jpeg', 'image/jpg'];
    if (file.size > 5000000)
      error.push(`${file.name}  is too large, please pick a smaller file`);
    if (types.every((type) => file.type !== type)) {
      error.push(`${file.type} is not a supported type`);
    }
    if (error.length < 1) {
      setImages((oldvalues) => ({
        ...oldvalues,
        [`image${id}`]: URL.createObjectURL(file)
      }));
      setImageData((oldvalues) => ({
        ...oldvalues,
        [`image${id}`]: file
      }));
    }
  };

  const onErrorImage = (id) => (event) => {
    setImages((oldvalues) => ({
      ...oldvalues,
      [`image${id}`]: ''
    }));

    setImageData((oldvalues) => ({
      ...oldvalues,
      [`image${id}`]: null
    }));
  };

  const getNumberImage = () => {
    let nb = 0;
    if (images.image0 !== '') nb++;
    if (images.image1 !== '') nb++;
    if (images.image2 !== '') nb++;
    if (images.image3 !== '') nb++;
    if (images.image4 !== '') nb++;
    return nb;
  };

  const removeImage = (id) => (event) => {
    setImages((oldvalues) => ({
      ...oldvalues,
      [`image${id}`]: ''
    }));

    setImageData((oldvalues) => ({
      ...oldvalues,
      [`image${id}`]: null
    }));
  };

  const handleSubmitImage = async (event) => {
    event.preventDefault();
    const data = new FormData();
    data.append('images', imageData.image0);
    data.append('images', imageData.image1);
    data.append('images', imageData.image2);
    data.append('images', imageData.image3);
    data.append('images', imageData.image4);

    data.append('arrayImage', images.image0);
    data.append('arrayImage', images.image1);
    data.append('arrayImage', images.image2);
    data.append('arrayImage', images.image3);
    data.append('arrayImage', images.image4);
    try {
      const response = await fetch('http://localhost:5000/settings/images', {
        method: 'POST',
        credentials: 'include',
        body: data
      });

      if (response.status !== 200) {
        throw new Error('fatalError');
      }
      const body = response.json();
      setMerror('successChangeSettings');
    } catch (error) {
      setMerror('imageError');
    }
  };
  //BIO FUNCTIONS

  const handleChangeBio = (event) => {
    event.persist();
    setBioForm((oldvalues) => ({ ...oldvalues, ['bio']: event.target.value }));
  };

  const handleSubmitBio = async (event) => {
    const data = JSON.stringify({
      tags: bioForm.tags,
      bio: bioForm.bio
    });

    try {
      const response = await fetch('http://localhost:5000/settings/bio', {
        method: 'POST',
        body: data,
        headers: new Headers({ 'content-type': 'application/json' })
      });
      if (response.status !== 200) {
        throw new error(body.merror);
      }
      const body = await response.json();
      setMerror('successChangeSettings');
    } catch (e) {
      setMerror('failChangeSettings');
    }
  };
  return (
    <Grid className="settingsFormContainer" item xs={12} md={6} xl={5}>
      {data === false && (
        <h2 className="errorMessage">Vous devez avoir au moins une photo</h2>
      )}
      <Tabs
        orientation="vertical"
        variant="scrollable"
        value={activeTab}
        onChange={handleChangeTab}
        aria-label="Vertical tabs settings form"
        className="verticalTabsSettings"
      // TabIndicatorProps={props}
      >
        <Tab
          label="Private informations"
          id="settingsTabs-0"
          className="categorieSettings"
        />
        <Tab
          label=" Public informations"
          id="settingsTabs-1"
          className="categorieSettings"
        />
        <Tab label="Images" id="settingsTabs-2" className="categorieSettings" />
        <Tab
          label="Bio and Tags"
          id="settingsTabs-3"
          className="categorieSettings"
        />
      </Tabs>
      <TabPanel value={activeTab} index={0} id="contentSettingsForm0">
        <form
          onSubmit={handleSubmitPrivate}
          method="POST"
          id="foform"
          autoComplete="on"
        >
          <div id="merrorSettings">{merror}</div>

          <Grid item xl={8} xs={12}>
            <FormControl
              className="settingsInputFormPrivate settingsInputForm"
              error={privateFormStatus.mail !== true}
            >
              <InputLabel htmlFor="mailInput" className="labelInputForm">
                Mail
              </InputLabel>
              <Input
                defaultValue={privateForm.mail}
                id="mailInputSettings"
                type="email"
                onBlur={handleChangeMail}
                className="settingsInput"
              />
            </FormControl>
          </Grid>
          <Grid item xl={8} xs={12}>
            <FormControl
              className="settingsInputFormPrivate settingsInputForm"
              error={privateFormStatus.password !== true}
            >
              <InputLabel htmlFor="passwordInput" className="labelInputForm">
                Password
              </InputLabel>
              <Input
                defaultValue={privateForm.password}
                id="passwordInputSettings"
                type="password"
                onBlur={handleChangePassword}
                className="settingsInput"
              />
            </FormControl>
          </Grid>
          <Grid item xl={8} xs={12}>
            <FormControl
              className="settingsInputFormPrivate settingsInputForm"
              error={privateFormStatus.confirm !== true}
            >
              <InputLabel
                htmlFor="passwordInputSettingsconfirm"
                className="labelInputForm"
              >
                Confirm password
              </InputLabel>
              <Input
                defaultValue={privateForm.confirm}
                id="passwordInputSettingsconfirm"
                type="password"
                onBlur={handleChangeConfirmPassword}
                className="settingsInput"
              />
            </FormControl>
          </Grid>
          <Grid item xl={8} xs={12}>
            <div>
              <Button
                variant="contained"
                id="resetButtonSettings"
                className="buttonSettings"
                color="primary"
                onClick={handleResetPassword}
              >
                Reset password
              </Button>
            </div>
          </Grid>
          <Grid item xl={8} xs={12} id="geoSettings">
            <FormControl
              component="fieldset"
              className="settingsInputFormPrivate settingsInputForm"
            >
              <FormGroup>
                <FormControlLabel
                  className="salsifi"
                  id="geostyle"
                  control={
                    <Switch
                      color="primary"
                      className="switchSettings"
                      checked={privateFormStatus.geo}
                      onChange={handleChangeGeo}
                      value="gilad"
                    />
                  }
                  label="Geolocalisation"
                />
              </FormGroup>
            </FormControl>
          </Grid>
          <div id="buttonSettingsPrivate">
            <Button
              variant="outlined"
              className="buttonSettings"
              onClick={handleSubmitPrivate}
              disabled={disableButtonPrivate()}
            >
              Confirm
            </Button>
          </div>
        </form>
      </TabPanel>
      <TabPanel value={activeTab} index={1} id="contentSettingsForm1">
        <form
          onSubmit={handleSubmitPublic}
          method="POST"
          id="formPublic"
          autoComplete="on"
        >
          <div id="merrorSettings">{merror}</div>

          <Grid item xl={8} xs={12}>
            <FormControl
              className="settingsInputForm settingsInputFormPublic"
              error={publicFormStatus.pseudo !== true}
            >
              <InputLabel htmlFor="pseudoInput" className="labelInputForm">
                Pseudo
              </InputLabel>
              <Input
                defaultValue={publicForm.pseudo}
                type="pseudo"
                onBlur={handleChangePseudo}
                className="settingsInput pseudoInputSettings"
              />
            </FormControl>
          </Grid>
          <Grid item xl={8} xs={12}>
            <FormControl
              className="settingsInputForm settingsInputFormPublic"
              error={publicFormStatus.firstName !== true}
            >
              <InputLabel htmlFor="pseudoInput" className="labelInputForm">
                First name
              </InputLabel>
              <Input
                defaultValue={publicForm.firstName}
                type="text"
                onBlur={handleChangeFirstName}
                className="settingsInput pseudoInputSettings"
              />
            </FormControl>
          </Grid>
          <Grid item xl={8} xs={12}>
            <FormControl
              className="settingsInputForm settingsInputFormPublic"
              error={publicFormStatus.lastName !== true}
            >
              <InputLabel htmlFor="pseudoInput" className="labelInputForm">
                Last name
              </InputLabel>
              <Input
                defaultValue={publicForm.lastName}
                type="text"
                onBlur={handleChangeLastName}
                className="settingsInput pseudoInputSettings"
              />
            </FormControl>
          </Grid>
          <Grid item xl={8} xs={12}>
            <FormControl className="settingsInputForm settingsInputFormPublic">
              <InputLabel htmlFor="targetInput">Sexual orientation</InputLabel>
              <Select
                className="firstInput"
                native
                value={publicForm.sexualOrientation}
                onChange={handleChangeSexualorientation}
                inputProps={{
                  id: 'targetInput',
                  name: 'Sexual Orientation'
                }}
              >
                <option value="N" />
                <option value="B">Bisexual</option>
                <option value="H">Heterosexual</option>
                <option value="F">Homosexual</option>
              </Select>
            </FormControl>
          </Grid>
          <Grid item xl={8} xs={12}>
            <FormControl className="settingsInputForm settingsInputFormPublic">
              <InputLabel htmlFor="genderInput">Gender</InputLabel>
              <Select
                className="firstInput"
                native
                value={publicForm.gender}
                onChange={handleChangeGenre}
                inputProps={{
                  id: 'genderInput',
                  name: 'Gender'
                }}
              >
                <option value="N" />
                <option value="H">Man</option>
                <option value="F">Woman</option>
              </Select>
            </FormControl>
          </Grid>
          <div id="buttonSettingsPrivate">
            <Button
              id="settingsPublicButton"
              variant="outlined"
              onClick={handleSubmitPublic}
              disabled={disableButtonPublic()}
            >
              Confirm
            </Button>
          </div>
        </form>
      </TabPanel>
      <TabPanel value={activeTab} index={2} id="contentSettingsForm2">
        <div id="merrorSettings">{merror}</div>
        <Grid item xl={12} xs={12} id="imageContainer">
          <Button
            variant="contained"
            color="primary"
            size="large"
            className="buttonSendImage"
            startIcon={<SaveIcon />}
            onClick={handleSubmitImage}
          >
            Send image
          </Button>
          {images.image0 !== '' && (
            <div id={`imageContainerSettings-0`} className="imageSettingsdiv">
              <ModalImage
                small={images.image0}
                large={images.image0}
                alt="user Image"
                hideDownload={true}
                className="imageUserSettings"
              />

              <div className="labelContainer">
                <label
                  htmlFor="inputImagesSettings0"
                  className="labelImage addImage cursor"
                >
                  <PublishOutlinedIcon />
                </label>
                <input
                  id="inputImagesSettings0"
                  type="file"
                  onChange={handleChangeImage(0)}
                  onError={onErrorImage(0)}
                  className="inputImageSettingss hide"
                />

                {/*//ug*/}
                <label
                  htmlFor="deleteImage0"
                  className="labelImage removeImage cursor"
                >
                  <DeleteForeverOutlinedIcon />
                </label>
              </div>
              <input
                className="hide"
                id="deleteImage0"
                type="button"
                onClick={removeImage(0)}
              />
            </div>
          )}
          {images.image1 !== '' && (
            <div id={`imageContainerSettings-1`} className="imageSettingsdiv">
              <ModalImage
                small={images.image1}
                large={images.image1}
                alt="user Image"
                hideDownload={true}
                className="imageUserSettings"
              />
              <div className="labelContainer">
                <label
                  htmlFor="inputImagesSettings1"
                  className="labelImage addImage cursor"
                >
                  <PublishOutlinedIcon />
                </label>
                <input
                  id="inputImagesSettings1"
                  type="file"
                  onChange={handleChangeImage(1)}
                  onError={onErrorImage(1)}
                  className="inputImageSettingss hide"
                />

                <label
                  htmlFor="deleteImage1"
                  className="labelImage removeImage cursor"
                >
                  <DeleteForeverOutlinedIcon />
                </label>
                <input
                  id="deleteImage1"
                  className="hide"
                  type="button"
                  onClick={removeImage(1)}
                />
              </div>
            </div>
          )}
          {images.image2 !== '' && (
            <div id={`imageContainerSettings-2`} className="imageSettingsdiv">
              <ModalImage
                small={images.image2}
                large={images.image2}
                alt="user Image"
                hideDownload={true}
                className="imageUserSettings"
              />
              <div className="labelContainer">
                <label
                  htmlFor="inputImagesSettings2"
                  className="labelImage addImage cursor"
                >
                  <PublishOutlinedIcon />
                </label>
                <input
                  id="inputImagesSettings2"
                  type="file"
                  onChange={handleChangeImage(2)}
                  onError={onErrorImage(2)}
                  className="inputImageSettingss hide"
                />
                <label
                  htmlFor="deleteImage2"
                  className="labelImage removeImage cursor"
                >
                  <DeleteForeverOutlinedIcon />
                </label>
                <input
                  type="button"
                  id="deleteImage2"
                  className="hide"
                  onClick={removeImage(2)}
                />
              </div>
            </div>
          )}
          {images.image3 !== '' && (
            <div id={`imageContainerSettings-3`} className="imageSettingsdiv">
              <ModalImage
                small={images.image3}
                large={images.image3}
                alt="user Image"
                hideDownload={true}
                className="imageUserSettings"
              />
              <div className="labelContainer">
                <label
                  htmlFor="inputImagesSettings3"
                  className="labelImage addImage cursor"
                >
                  <PublishOutlinedIcon />
                </label>
                <input
                  id="inputImagesSettings3"
                  type="file"
                  onChange={handleChangeImage(3)}
                  onError={onErrorImage(3)}
                  className="inputImageSettingss hide"
                />
                <label
                  htmlFor="deleteImage3"
                  className="labelImage removeImage cursor"
                >
                  <DeleteForeverOutlinedIcon />
                </label>
                <input
                  id="deleteImage3"
                  type="button"
                  onClick={removeImage(3)}
                  className="hide"
                />
              </div>
            </div>
          )}
          {images.image4 !== '' && (
            <div id="imageContainerSettings-4" className="imageSettingsdiv">
              <ModalImage
                small={images.image4}
                large={images.image4}
                alt="user Image"
                hideDownload={true}
                className="imageUserSettings"
              />
              <div className="labelContainer">
                <label
                  htmlFor="inputImagesSettings4"
                  className="labelImage addImage cursor"
                >
                  <PublishOutlinedIcon />
                </label>
                <input
                  id="inputImagesSettings4"
                  type="file"
                  onChange={handleChangeImage(4)}
                  onError={onErrorImage(4)}
                  className="inputImageSettingss hide"
                />
                <label
                  htmlFor="deleteImage4"
                  className="labelImage removeImage cursor"
                >
                  <DeleteForeverOutlinedIcon />
                </label>
                <input
                  type="button"
                  onClick={removeImage(4)}
                  className="hide"
                  id="deleteImage4"
                />
              </div>
            </div>
          )}
          {getNumberImage() < 5 && (
            <div>
              <label htmlFor="inputAddImage">
                <AddCircleOutlineOutlinedIcon />
              </label>
              <input
                id="inputAddImage"
                type="file"
                className="hide"
                onChange={handleChangeImage(-1)}
                onError={() => {
                  return null;
                }}
              />
            </div>
          )}
        </Grid>
      </TabPanel>
      <TabPanel value={activeTab} index={3} id="contentSettingsForm3">
        <SettingsTags userTags={userTags} />
      </TabPanel>
    </Grid>
  );
};

SettingsForm.getInitialProps = async (ctx) => { };

export default SettingsForm;
