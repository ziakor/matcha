import React from 'react';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import fetch from 'isomorphic-unfetch';
import SignupImages from './SignupForm/SignupImages';
import SignupPersonnalInfos from './SignupForm/SignupPersonnalInfos';
import SignupPrivateInfos from './SignupForm/SignupPrivateInfos';
import SignupTags from './SignupForm/SignupBioTags';
import { CircularProgress } from '@material-ui/core';
import Router from 'next/router';

const SignupContainer = () => {
  const [activeStep, setActiveStep] = React.useState(0);
  const [errorStep, setErrorStep] = React.useState([
    false,
    false,
    false,
    false
  ]);
  const [skipped, setSkipped] = React.useState(new Set());
  const [loading, setLoading] = React.useState(false);
  const [Forms, setForms] = React.useState({
    firstName: '',
    lastName: '',
    pseudo: '',
    age: null,
    genre: '',
    target: '',
    mail: '',
    passwd: '',
    bio: '',
    tags: [''],
    images: ['', '', '', '', ''],
    geo: [false, 0, 0]
  });
  const [endMessage, setEndMessage] = React.useState(['default']);
  const [statusForms, setStatusForms] = React.useState({
    step1: {
      mail: false,
      password: false
    },
    step2: {
      pseudo: false,
      firstName: false,
      lastName: false,
      age: false,
      genre: false,
      target: false
    }
  });

  const steps = [
    'Private informations',
    'Public informations',
    'Images',
    'Bio and Tags'
  ];
  const handleNext = async () => {
    let newSkipped = skipped;
    if (activeStep === 0) {
      if (
        statusForms.step1.mail === false ||
        statusForms.step1.password === false
      ) {
        setErrorStep((oldvalues) => ({ ...oldvalues, [activeStep]: true }));
        return;
      } else {
        setErrorStep((oldvalues) => ({ ...oldvalues, [activeStep]: false }));
      }
    }
    if (activeStep === 1) {
      if (
        statusForms.step2.pseudo === false ||
        statusForms.step2.firstName === false ||
        statusForms.step2.lastName === false ||
        statusForms.step2.genre === false ||
        statusForms.step2.target === false
      ) {
        setErrorStep((oldvalues) => ({ ...oldvalues, [activeStep]: true }));
        return;
      } else {
        setErrorStep((oldvalues) => ({ ...oldvalues, [activeStep]: false }));
      }
    }
    if (activeStep === 2) {
      newSkipped = new Set(newSkipped.values());
      newSkipped.delete(activeStep);
    }
    if (checkStepValidity(activeStep) === 1) {
      setErrorStep((oldvalue) => {
        oldvalue[activeStep] = true;
        return oldvalue;
      });
    }
    if (activeStep < 4) {
      setActiveStep((prevActiveStep) => prevActiveStep + 1);
    }
    setSkipped(newSkipped);
    if (activeStep + 1 === 4) {
      let error = 0;
      setLoading(true);
      if (checkStepValidity(0) === 1) {
        setErrorStep((oldvalue) => {
          oldvalue[0] = true;
          return oldvalue;
        });
        error = 1;
      }
      if (checkStepValidity(1) === 1) {
        setErrorStep((oldvalue) => {
          oldvalue[1] = true;
          return oldvalue;
        });
        error = 1;
      }
      if (error === 0 || error === 1) {
        const res = await signinToServer();
        if (res.merror.length > 0) setEndMessage(res.merror);
        else {
          setEndMessage(['completeMessage']);
          setTimeout(() => { }, 2000);
          Router.push('/signin');
        }
        setLoading(false);
      } else {
        setEndMessage(['stepError']);
        setLoading(false);
      }
    }
  };

  const signinToServer = async () => {
    const data = new FormData();
    data.append('target', Forms.target);
    data.append('mail', Forms.mail);
    data.append('passwd', Forms.passwd);
    data.append('pseudo', Forms.pseudo);
    data.append('firstName', Forms.firstName);
    data.append('lastName', Forms.lastName);
    data.append('age', Forms.age);
    data.append('genre', Forms.genre);
    data.append('bio', Forms.bio);
    data.append('geo', Forms.geo[0]);
    data.append('geo', Forms.geo[1]);
    data.append('geo', Forms.geo[2]);
    data.append('images', Forms.images[0]);
    data.append('images', Forms.images[1]);
    data.append('images', Forms.images[2]);
    data.append('images', Forms.images[3]);
    data.append('images', Forms.images[4]);
    for (let index = 0; index < Forms.tags.length; index++) {
      data.append('tags', Forms.tags[index].value);
    }

    try {
      const response = await fetch('http://localhost:5000/user/signup', {
        method: 'POST',
        body: data
      });
      const body = await response.json();
      setEndMessage(['...']);
      return body;
    } catch (err) {
      return { merror: ['fatalError'] };
    }
  };

  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
    setErrorStep((oldvalue) => {
      oldvalue[activeStep - 1] = false;
      return oldvalue;
    });
  };

  const handleSkip = () => {
    if (activeStep !== 2 && activeStep !== 3) {
      // You probably want to guard against something like this,
      // it should never occur unless someone's actively trying to break something.
      throw new Error("You can't skip a step that isn't optional.");
    }

    setActiveStep((prevActiveStep) => prevActiveStep + 1);
    setSkipped((prevSkipped) => {
      const newSkipped = new Set(prevSkipped.values());
      newSkipped.add(activeStep);
      return newSkipped;
    });
  };

  const checkStepValidity = (id) => {
    let error = 0;
    switch (id) {
      case 0:
        if (Forms.mail === '' || Forms.passwd === '') {
          error = 1;
        }
        break;
      case 1:
        if (
          Forms.pseudo === '' ||
          Forms.firstName === '' ||
          Forms.lastName === '' ||
          isNaN(Forms.age) ||
          Forms.genre === ''
        ) {
          error = 1;
        }
      default:
        break;
    }
    return error;
  };
  const changeValueErrorStep = (data) => {
    setErrorStep((oldValues) => {
      oldValues[activeStep] = data;
      return oldValues;
    });
  };

  return (
    <div>
      <Stepper id="stepperSignup" activeStep={activeStep}>
        {steps.map((label, index) => {
          const stepProps = {};
          const labelProps = { error: errorStep[index] };

          return (
            <Step className=" stepLabel" key={label} {...stepProps}>
              <StepLabel
                {...labelProps}
                icon={index + 1}
                className="colorLabel"
                id="bite"
              >
                {label}
              </StepLabel>
            </Step>
          );
        })}
      </Stepper>
      <div id="signupFormContent">
        {activeStep === 0 && (
          <SignupPrivateInfos
            setForms={setForms}
            formsData={Forms}
            setStatusForms={setStatusForms}
          />
        )}
        {activeStep === 1 && (
          <SignupPersonnalInfos
            setForms={setForms}
            formsData={Forms}
            setStatusForms={setStatusForms}
          />
        )}
        {activeStep === 2 && (
          <SignupImages setForms={setForms} formsData={Forms} />
        )}
        {activeStep === 3 && (
          <SignupTags setForms={setForms} formsData={Forms} />
        )}
        {activeStep === 4 && (
          <React.Fragment>
            {loading && <CircularProgress />}
            {endMessage.map((value, index) => (
              <div key={`${value}-${index}`}>{value}</div>
            ))}
          </React.Fragment>
        )}
      </div>
      <div id="divButtonSignup">
        <Button
          disabled={activeStep === 0}
          variant="contained"
          onClick={handleBack}
          id="backButtonSignup"
          className="bbuton colorLabel"
        >
          Back
        </Button>

        {activeStep < 4 && endMessage !== 'Success' && (
          <Button
            variant="contained"
            color="primary"
            id="nextButtonSignup"
            onClick={handleNext}
            className="bbuton colorLabel"
          >
            {activeStep === steps.length - 1 ? 'Finish' : 'Next'}
          </Button>
        )}
      </div>
    </div>
  );
};

SignupContainer.getInitialProps = async () => { };

export default SignupContainer;
