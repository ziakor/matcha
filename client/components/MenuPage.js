import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import Link from 'next/link';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import Switch from '@material-ui/core/Switch';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import NotificationsNoneOutlinedIcon from '@material-ui/icons/NotificationsNoneOutlined';
import NotificationsActiveOutlinedIcon from '@material-ui/icons/NotificationsActiveOutlined';

import Cookies from 'universal-cookie';
import io from 'socket.io-client';

import Router from 'next/router';

const cookies = new Cookies();
const MenuPage = ({ notifs, authProps }) => {
  const [auth, setAuth] = React.useState(authProps);
  const [anchorPage, setAnchorPage] = React.useState(null);
  const [language, setLanguage] = React.useState(false);
  const [mount, setMount] = useState(false);
  const open = Boolean(anchorPage);
  const [anchorNotif, setAnchorNotif] = useState(null);

  const openNotif = Boolean(anchorNotif);
  const [dataNotif, setDataNotif] = useState({
    nbNotif: notifs !== undefined ? notifs.length : 0,
    list: notifs
  });
  const [nbNotif, setNbNotif] = useState(
    notifs !== undefined ? notifs.length : 0
  );
  const [List, setList] = useState(notifs);
  if (authProps === true) {
    const socket = io.connect('http://localhost:5001', {
      autoConnect: true,
      transports: ['websocket'],
      query: 'tokenCookie=' + cookies.get('tokenCookie')
    });

    socket.on('notif', (data) => {
      if (data !== undefined) {
        setNbNotif((oldvalues) => {
          return oldvalues + 1;
        });
        setList((oldvalues) => {
          let newTab = oldvalues;
          newTab.push([data.pseudo, data.type, 'ME']);
          return newTab;
        });

        setAnchorNotif(null);
      }
    });
  }

  const handleMenu = (event) => {
    setAnchorPage(event.currentTarget);
  };
  const handleNotif = async (event) => {
    event.persist();

    setAnchorNotif(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorPage(null);
  };
  const handleCloseNotif = async () => {
    try {
      await fetch('http://localhost:5000/notifications/delete', {
        method: 'PUT',
        credentials: 'include'
      });
    } catch (e) {
    }
    setList([]);
    setNbNotif(0);
    setAnchorNotif(null);
  };
  const handleLogOut = async () => {
    try {
      const response = await fetch('http://localhost:5000/user/logout', {
        method: 'GET',
        headers: {
          authorization: cookies.get('tokenCookie')
        }
      });
      if (response.status === 200) {
      }
    } catch (e) {
    }
    while (cookies.get('tokenCookie')) {
      cookies.remove('tokenCookie');
    }
    await Router.push('/');
  };
  const handleSwitchLanguage = () => { };

  useEffect(() => {
    return (() => { })
  }, [])
  return (
    <React.Fragment>
      {auth && (
        <div>
          {nbNotif === 0 ? (
            <React.Fragment>
              <IconButton
                id="MenuNotif"
                aria-label="notificationButton"
                aria-controls="MenuNotif"
                aria-haspopup="true"
                onClick={handleNotif}
                color="inherit"
              >
                <NotificationsNoneOutlinedIcon />
              </IconButton>
              <Menu
                id="MenuNotif"
                anchorEl={anchorNotif}
                anchorOrigin={{
                  vertical: 'top',
                  horizontal: 'left'
                }}
                keepMounted
                transformOrigin={{
                  vertical: 'top',
                  horizontal: 'left'
                }}
                open={openNotif}
                onClose={handleCloseNotif}
              >
                <div id="menuNotifPop">No notification</div>
              </Menu>
            </React.Fragment>
          ) : (
              <React.Fragment>
                <IconButton
                  id="MenuNotif"
                  aria-label="notificationButton"
                  aria-controls="MenuNotif"
                  aria-haspopup="true"
                  onClick={handleNotif}
                  color="inherit"
                >
                  <NotificationsActiveOutlinedIcon />
                </IconButton>
                <Menu
                  anchorEl={anchorNotif}
                  anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'left'
                  }}
                  keepMounted
                  transformOrigin={{
                    vertical: 'top',
                    horizontal: 'left'
                  }}
                  open={openNotif}
                  onClose={handleCloseNotif}
                >
                  <div id="menuNotifPop">
                    {List.map((values, index) => {
                      return <div key={`${values[0]}-${index}`}>{values[0]} <span className="notifIn">{values[1]}</span></div>;
                    })}
                  </div>
                </Menu>
              </React.Fragment>
            )}
        </div>
      )}
      <IconButton
        id="MenuTestHome"
        aria-label="list of all pages"
        aria-controls="menu-page"
        aria-haspopup="true"
        onClick={handleMenu}
        color="inherit"
      >
        <MenuIcon />
      </IconButton>
      <Menu
        id="menu-page"
        anchorEl={anchorPage}
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'right'
        }}
        keepMounted
        transformOrigin={{
          vertical: 'top',
          horizontal: 'right'
        }}
        open={open}
        onClose={handleClose}
      >
        <div>
          <Link href="/">
            <MenuItem onClick={handleClose}>Home</MenuItem>
          </Link>
        </div>
        {!auth && (
          <div>
            <Link href="/signup">
              <MenuItem onClick={handleClose}>Sign up</MenuItem>
            </Link>
            <Link href="/signin">
              <MenuItem onClick={handleClose}>Sign in</MenuItem>
            </Link>
          </div>
        )}
        {auth && (
          <div>
            <Link href="/match">
              <MenuItem onClick={handleClose}>Match</MenuItem>
            </Link>
            <Link href="/search">
              <MenuItem onClick={handleClose}>Search</MenuItem>
            </Link>
            <Link href="/messenger">
              <MenuItem onClick={handleClose}>Messenger</MenuItem>
            </Link>
            <hr />
            <Link href="/myprofile">
              <MenuItem onClick={handleClose}>My profile</MenuItem>
            </Link>
            <Link href="/settings">
              <MenuItem onClick={handleClose}>Settings</MenuItem>
            </Link>
            <MenuItem onClick={handleLogOut}>Log out</MenuItem>
          </div>
        )}
      </Menu>
    </React.Fragment>
  );
};

MenuPage.getInitialProps = async () => { };

export default MenuPage;
