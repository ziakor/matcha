import React from 'react';
import PropTypes from 'prop-types';
import Head from 'next/head';

const Customhead = ({ title }) => (
  <React.Fragment>
    <Head>
      <title>{title}</title>

      <meta
        name="viewport"
        content="minimum-scale=1, initial-scale=1, width=device-width, shrink-to-fit=no"
      />
      <link
        rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap"
      />
      <link
        rel="stylesheet"
        href="https://fonts.googleapis.com/icon?family=Material+Icons"
      />
    </Head>
  </React.Fragment>
);

export default Customhead;
