/* eslint-disable jsx-a11y/anchor-is-valid */
import React from 'react';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Link from 'next/link';

const HomeButton = ({ t }) => (
  <React.Fragment>
    <div id="homeContainer">
      <div id="homeTextContainer">
        <Typography id="HomeText" variant="h1">
          Match. Chat. Date.
        </Typography>
        <Typography id="HomeText2" variant="h1">
          Easy as Natural
        </Typography>
        <Typography id="HomeText3" variant="h1">
          Selection.
        </Typography>
      </div>
      <div id="homebuttonContainer">
        <Link href="/signup" as="/signup">
          <Button
            id="Button1"
            variant="contained"
            className="buttonHome"
            fullWidth
          >
            Sign up
          </Button>
        </Link>
        <Link href="/signin" as="/signin">
          <Button
            id="Button2"
            variant="contained"
            className="buttonHome"
            fullWidth
          >
            Sign in
          </Button>
        </Link>
      </div>
    </div>
  </React.Fragment>
);

export default HomeButton;
