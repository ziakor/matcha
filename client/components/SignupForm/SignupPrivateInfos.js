import React from 'react';
import Typography from '@material-ui/core/Typography';
import Link from 'next/link';
import Grid from '@material-ui/core/Grid';
import {
  FormControl,
  InputLabel,
  FormGroup,
  FormControlLabel,
  Input,
  FormHelperText,
  Switch
} from '@material-ui/core';

const SignupPrivateInfos = ({ setForms, formsData, setStatusForms }) => {
  const [mailStatus, setMailStatus] = React.useState(true);
  const [passwordStatus, setPasswordStatus] = React.useState({
    status: true,
    data: ''
  });
  const [
    passwordConfirmationStation,
    setPasswordConfirmationStatus
  ] = React.useState(true);
  const handleCheckMail = (event) => {
    event.preventDefault();
    const str = event.target.value;
    const regex = /\S+@\S+\.\S+$/;
    if (regex.test(str)) {
      setForms((oldValues) => ({ ...oldValues, ['mail']: str }));
      setStatusForms((oldvalues) => {
        oldvalues.step1.mail = true;
        return oldvalues;
      });
      setMailStatus(true);
    } else {
      setMailStatus(false);

      setForms((oldValues) => ({ ...oldValues, ['mail']: null }));
      setStatusForms((oldvalues) => {
        oldvalues.step1.mail = false;
        return oldvalues;
      });
    }
  };

  const handleCheckPassword = (event) => {
    event.preventDefault();
    const passwd = event.target.value;
    const regex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/;

    if (regex.test(passwd)) {
      setPasswordStatus({ status: true, data: passwd });
    } else {
      setPasswordStatus({ status: false, data: '' });
    }
  };

  const handleCheckConfirmationPassword = (event) => {
    event.preventDefault();
    const passwd = event.target.value;
    const regex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/;
    if (regex.test(passwd) && passwd === passwordStatus.data) {
      setPasswordConfirmationStatus(true);
      setForms((oldValues) => ({ ...oldValues, ['passwd']: passwd }));
      setStatusForms((oldvalues) => {
        oldvalues.step1.password = true;
        return oldvalues;
      });
    } else {
      setPasswordConfirmationStatus(false);
      setForms((oldValues) => ({ ...oldValues, ['passwd']: null }));
      setStatusForms((oldvalues) => {
        oldvalues.step1.password = false;
        return oldvalues;
      });
    }
  };

  const handleSwitchGeo = () => {
    if (formsData.geo[0] === false) {
      navigator.geolocation.getCurrentPosition(
        (position) => {
          setForms((oldvalues) => ({
            ...oldvalues,
            geo: [true, position.coords.latitude, position.coords.longitude]
          }));
        },
        null,
        { enableHighAccuracy: true, maximumAge: 0 }
      );
    } else {
      setForms((oldvalues) => ({ ...oldvalues, geo: [false, 0, 0] }));
    }
  };
  return (
    <React.Fragment>
      <Grid item xl={6}>
        <FormControl
          className="signupInputForm"
          required
          error={mailStatus === true ? false : true}
        >
          <InputLabel htmlFor="mailInput">Mail</InputLabel>
          <Input
            defaultValue={formsData.mail}
            id="mailInput"
            type="email"
            onBlur={handleCheckMail}
            className="firstInput"
          />
        </FormControl>
      </Grid>
      <Grid item xl={6} xs={12}>
        <FormControl
          className="signupInputForm firstInputForm"
          required
          error={passwordStatus.status !== true}
        >
          <InputLabel htmlFor="passwordInputd">Password</InputLabel>
          <Input
            defaultValue={formsData.passwd}
            id="passwordInputd"
            type="password"
            className="firstInput"
            onBlur={handleCheckPassword}
          />
        </FormControl>
      </Grid>

      <Grid item xl={6} xs={12}>
        <FormControl
          className="signupInputForm firstInputForm"
          required
          error={passwordConfirmationStation !== true}
        >
          <InputLabel
            htmlFor="passwordConfirmationInput"
            id="confirmationPasswordInput"
          >
            Password confirmatiom
          </InputLabel>
          <Input
            defaultValue={formsData.passwd}
            id="passwordConfirmationInput"
            type="password"
            className="firstInput"
            onBlur={handleCheckConfirmationPassword}
          />
        </FormControl>
        <FormHelperText id="helperpassword">
          passwords must contain at least 8 characters including a capital
          letter, a number and a special character!
        </FormHelperText>
      </Grid>
      <Grid item xl={6} xl={12}>
        <FormControl component="fieldset">
          <FormGroup>
            <FormControlLabel
              className="salsifi"
              id="geostyle"
              control={
                <Switch
                  color="primary"
                  checked={formsData.geo[0]}
                  onChange={handleSwitchGeo}
                  value="gilad"
                />
              }
              label="Geolocalisation"
            />
          </FormGroup>
        </FormControl>
      </Grid>
      <Grid item xl={6} xl={12}>
        <Typography id="already" align="center" color="primary" variant="h1">
          Already have an account?
          <Link href="/signin">
            <a> Sign in </a>
          </Link>
        </Typography>
      </Grid>
    </React.Fragment>
  );
};

export default SignupPrivateInfos;
