/* eslint-disable jsx-a11y/anchor-is-valid */
import React from 'react';

const SignupImages = ({ setForms }) => {
  const [images, setImages] = React.useState({
    image0: './static/images/image1.jpg',
    image1: './static/images/image1.jpg',
    image2: './static/images/image1.jpg',
    image3: './static/images/image1.jpg',
    image4: './static/images/image1.jpg'
  });

  const handleChangeImage = (id) => (event) => {
    const error = [];
    const file = event.target.files[0];
    const reader = new FileReader();
    const types = ['image/png', 'image/jpeg', 'image/jpg'];
    if (file.size > 5000000)
      error.push(`${file.name}  is too large, please pick a smaller file`);
    if (types.every((type) => file.type !== type)) {
      error.push(`${file.type} is not a supported type`);
    }
    setImages((oldValues) => ({
      ...oldValues,
      [`image${id}`]: URL.createObjectURL(file)
    }));
    console.log(file.size);
    setForms((oldValues) => {
      let data = oldValues;
      data.images[id] = file;
      return data;
    });
  };

  const onError = (id) => (event) => {
    setImages((oldValues) => ({
      ...oldValues,
      [`images${id}`]: './static/images/image1.jpg'
    }));
    setForms((oldValues) => {
      let data = oldValues;
      data.images[id] = '';
      return data;
    });
  };
  return (
    <div id="imageContainer">
      <label htmlFor="inputImage1">
        <img className="imageSigninForm" id="imgSignin1" src={images.image0} />
      </label>
      <input
        id="inputImage1"
        className="InputHiddenSigningForm"
        type="file"
        onChange={handleChangeImage(0)}
        onError={onError(0)}
      />
      <label htmlFor="inputImage2">
        <img className="imageSigninForm" id="imgSignin2" src={images.image1} />
      </label>
      <input
        id="inputImage2"
        className="InputHiddenSigningForm"
        type="file"
        onChange={handleChangeImage(1)}
        onError={onError(1)}
      />
      <label htmlFor="inputImage3">
        <img className="imageSigninForm" id="imgSignin3" src={images.image2} />
      </label>
      <input
        id="inputImage3"
        className="InputHiddenSigningForm"
        type="file"
        onChange={handleChangeImage(2)}
        onError={onError(2)}
      />
      <label htmlFor="inputImage4">
        <img className="imageSigninForm" id="imgSignin4" src={images.image3} />
      </label>
      <input
        id="inputImage4"
        className="InputHiddenSigningForm"
        type="file"
        onChange={handleChangeImage(3)}
        onError={onError(3)}
      />
      <label htmlFor="inputImage5">
        <img className="imageSigninForm" id="imgSignin5" src={images.image4} />
      </label>
      <input
        id="inputImage5"
        className="InputHiddenSigningForm"
        type="file"
        onChange={handleChangeImage(4)}
        onError={onError(4)}
      />
    </div>
  );
};

export default SignupImages;
