/* eslint-disable jsx-a11y/anchor-is-valid */
import React from 'react';
import Grid from '@material-ui/core/Grid';
import { FormControl, InputLabel, Input } from '@material-ui/core';
import makeAnimated from 'react-select/animated';
import Select from 'react-select';

const options = [
  { value: 'Rap', label: 'Rap', color: '#000000' },
  { value: 'RnB', label: 'RnB', color: '#000000' },
  { value: 'Chiens', label: 'Chiens', color: '#000000' },
  { value: 'Chats', label: 'Chats', color: '#000000' },
  { value: 'Cinema', label: 'Cinema', color: '#000000' },
  { value: 'Tarantino', label: 'Tarantino', color: '#000000' },
  { value: 'Marvel', label: 'Marvel', color: '#000000' },
  { value: 'Skateboard', label: 'Skateboard', color: '#000000' },
  { value: 'Traveling', label: 'Traveling', color: '#000000' },
  { value: 'Paris', label: 'Paris', color: '#000000' },
  { value: 'Bordeaux', label: 'Bordeaux', color: '#000000' },
  { value: 'Foot', label: 'Foot', color: '#000000' },
  { value: 'Coding', label: 'Coding', color: '#000000' },
  { value: '42Born2Code', label: '42Born2Code', color: '#000000' },
  { value: 'Food', label: 'Food', color: '#000000' },
  { value: 'Sport', label: 'Sport', color: '#000000' },
  { value: 'Google', label: 'Google', color: '#000000' },
  { value: 'Beer', label: 'Beer', color: '#000000' },
  { value: 'Tuning', label: 'Tuning', color: '#000000' },
  { value: 'Pringles', label: 'Pringles', color: '#000000' }
];
const animatedComponents = makeAnimated();

const SignupBioTags = ({ setForms, formsData }) => {
  const handleTags = (values) => {
    setForms((oldValues) => ({ ...oldValues, ['tags']: values }));
  };
  const handleBio = (event) => {
    event.persist();
    if (event.target.value.length > 160) {
      setForms((oldValues) => ({ ...oldValues, ['bio']: '' }));
    } else {
      setForms((oldValues) => ({ ...oldValues, ['bio']: event.target.value }));
    }
  };
  return (
    <React.Fragment>
      <Grid item xl={3} xs={12}>
        <Select
          label="Tags"
          className="firstInput colorLabel"
          closeMenuOnSelect={false}
          components={animatedComponents}
          onChange={handleTags}
          defaultValue={formsData.tags}
          isMulti
          options={options}
        />
      </Grid>
      <Grid item xl={3} xs={12}>
        <FormControl className="signupInputForm">
          <InputLabel htmlFor="bioInput">Bio</InputLabel>
          <Input
            defaultValue={formsData.bio}
            id="bioInput"
            type="text"
            onChange={handleBio}
          />
        </FormControl>
      </Grid>
    </React.Fragment>
  );
};

export default SignupBioTags;
