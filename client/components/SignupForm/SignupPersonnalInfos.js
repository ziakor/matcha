/* eslint-disable jsx-a11y/anchor-is-valid */
import React from 'react';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import {
  FormControl,
  InputLabel,
  Input,
  Select,
  FormHelperText
} from '@material-ui/core';

const SignupPersonnalInfos = ({ setForms, formsData, setStatusForms }) => {
  const [firstNameStatus, setFirstNameStatus] = React.useState(true);
  const [lastNameStatus, setLastNameStatus] = React.useState(true);
  const [pseudoStatus, setPseudoStatus] = React.useState(true);
  const [ageStatus, setAgeStatus] = React.useState(true);
  const [genreStatus, setGenreStatus] = React.useState({
    genre: '',
    status: true
  });
  const [targetStatus, setTargetStatus] = React.useState({
    target: '',
    status: true
  });

  const handleckCheckFirstName = (event) => {
    event.persist();
    if (event.target.value.length > 0) {
      setFirstNameStatus(true);
      setForms((oldValues) => ({
        ...oldValues,
        ['firstName']: event.target.value
      }));
      setStatusForms((oldvalues) => {
        oldvalues.step2.firstName = true;
        return oldvalues;
      });
    } else {
      setFirstNameStatus(false);
      setForms((oldValues) => ({
        ...oldValues,
        ['firstName']: null
      }));
      setStatusForms((oldvalues) => {
        oldvalues.step2.firstName = false;
        return oldvalues;
      });
    }
  };

  const handleckCheckLastName = (event) => {
    event.persist();
    if (event.target.value.length > 0) {
      setLastNameStatus(true);
      setForms((oldValues) => ({
        ...oldValues,
        ['lastName']: event.target.value
      }));
      setStatusForms((oldvalues) => {
        oldvalues.step2.lastName = true;
        return oldvalues;
      });
    } else {
      setLastNameStatus(false);
      setForms((oldValues) => ({
        ...oldValues,
        ['lastName']: null
      }));
      setStatusForms((oldvalues) => {
        oldvalues.step2.lastName = false;
        return oldvalues;
      });
    }
  };

  const handleCheckPseudo = (event) => {
    event.persist();
    const regexpseudo = /^[a-zA-Z]{4,15}$/;
    if (regexpseudo.test(event.target.value)) {
      setPseudoStatus(true);
      setForms((oldValues) => ({
        ...oldValues,
        ['pseudo']: event.target.value
      }));
      setStatusForms((oldvalues) => {
        oldvalues.step2.pseudo = true;
        return oldvalues;
      });
    } else {
      setPseudoStatus(false);
      setForms((oldValues) => ({
        ...oldValues,
        ['pseudo']: null
      }));
      setStatusForms((oldvalues) => {
        oldvalues.step2.pseudo = false;
        return oldvalues;
      });
    }
  };

  const handleChangeAge = (event) => {
    if (event.target.value >= 18 && event.target.value <= 99) {
      setAgeStatus(true);
      setForms((oldValues) => ({ ...oldValues, ['age']: event.target.value }));
      setStatusForms((oldvalues) => {
        oldvalues.step2.age = true;
        return oldvalues;
      });
    } else {
      setAgeStatus(false);
      setForms((oldValues) => ({ ...oldValues, ['age']: null }));
      setStatusForms((oldvalues) => {
        oldvalues.step2.age = false;
        return oldvalues;
      });
    }
  };

  const handleChangeGenre = (event) => {
    event.persist();
    if (event.target.value === 'H' || event.target.value === 'F') {
      setForms((oldValues) => ({
        ...oldValues,
        ['genre']: event.target.value
      }));
      setGenreStatus({ genre: event.target.value, status: true });
      setStatusForms((oldvalues) => {
        oldvalues.step2.genre = true;
        return oldvalues;
      });
    } else {
      setGenreStatus((oldValues) => ({ ...oldValues, ['status']: false }));
      setForms((oldValues) => ({
        ...oldValues,
        ['genre']: ''
      }));
      setStatusForms((oldvalues) => {
        oldvalues.step2.genre = false;
        return oldvalues;
      });
    }
  };

  const handleChangeTarget = (event) => {
    event.persist();
    if (
      event.target.value === 'H' ||
      event.target.value === 'F' ||
      event.target.value === 'B'
    ) {
      setForms((oldValues) => ({
        ...oldValues,
        ['target']: event.target.value
      }));
      setTargetStatus({ target: event.target.value, status: true });
      setStatusForms((oldvalues) => {
        oldvalues.step2.target = true;
        return oldvalues;
      });
    } else {
      setTargetStatus((oldValues) => ({ ...oldValues, ['status']: false }));
      setForms((oldValues) => ({
        ...oldValues,
        ['target']: null
      }));
      setStatusForms((oldvalues) => {
        oldvalues.step2.target = false;
        return oldvalues;
      });
    }
  };
  return (
    <React.Fragment>
      <Grid item xl={3} xs={12}>
        <FormControl
          className="signupInputFor"
          required
          error={pseudoStatus !== true}
        >
          <InputLabel htmlFor="pseudoInputSignup " className="colorLabel">
            Pseudo
          </InputLabel>
          <Input
            id="pseudoInputSignup"
            defaultValue={formsData.pseudo}
            type="text"
            onBlur={handleCheckPseudo}
            className="firstInput"
          />
          <FormHelperText className="colorLabel">
            Between 4 and 15 letters
          </FormHelperText>
        </FormControl>
      </Grid>
      <Grid item xl={3} xs={12}>
        <FormControl
          className="signupInputForm"
          required
          error={firstNameStatus !== true}
        >
          <InputLabel htmlFor="firstNameInput" className="colorLabel">
            First name
          </InputLabel>
          <Input
            defaultValue={formsData.fir}
            id="mailInput"
            type="text"
            onBlur={handleckCheckFirstName}
            className="firstInput"
          />
        </FormControl>
      </Grid>

      <Grid item xl={3} xs={12}>
        <FormControl
          className="signupInputForm"
          required
          error={lastNameStatus !== true}
        >
          <InputLabel htmlFor="lastNameInput" className="colorLabel">
            Last name
          </InputLabel>
          <Input
            defaultValue={formsData.lastName}
            id="lastNameInput"
            type="text"
            onBlur={handleckCheckLastName}
            className="firstInput"
          />
        </FormControl>
      </Grid>

      <Grid item xl={3} xs={12}>
        <FormControl
          required
          className="signupInputForm"
          error={ageStatus !== true}
        >
          <InputLabel htmlFor="ageInput" className="colorLabel">
            Âge
          </InputLabel>
          <Input
            id="ageInput"
            type="text"
            defaultValue={formsData.age}
            onBlur={handleChangeAge}
            className="firstInput"
          />
          <FormHelperText className="colorLabel">
            Between 18 and 99
          </FormHelperText>
        </FormControl>
      </Grid>
      <Grid item xl={3} xs={12}>
        <FormControl required className="signupInputForm">
          <InputLabel htmlFor="genreInput" className="colorLabel">
            Gender
          </InputLabel>
          <Select
            className="firstInput"
            native
            value={formsData.genre}
            onChange={handleChangeGenre}
            inputProps={{
              id: 'genreInput',
              name: 'genre'
            }}
          >
            <option value="N" />
            <option value="H">Man</option>
            <option value="F">Woman</option>
          </Select>
          <FormHelperText>Required</FormHelperText>
        </FormControl>
      </Grid>
      <Grid item xl={3} xs={12}>
        <FormControl className="signupInputForm">
          <InputLabel htmlFor="targetInput">Sexual orientation</InputLabel>
          <Select
            className="firstInput"
            native
            value={formsData.target}
            onChange={handleChangeTarget}
            inputProps={{
              id: 'targetInput',
              name: 'sexual orientation'
            }}
          >
            <option value="N" />
            <option value="B">Bisexuel</option>
            <option value="H">Heteroxual</option>
            <option value="F">Homosexual</option>
          </Select>
        </FormControl>
      </Grid>
    </React.Fragment>
  );
};

SignupPersonnalInfos.getInitialProps = async () => {};

SignupPersonnalInfos.propTypes = {
  setForms: PropTypes.func.isRequired,
  setStatusForms: PropTypes.func.isRequired
};

export default SignupPersonnalInfos;
