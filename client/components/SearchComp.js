import React, { useState } from 'react';
import PropTypes from 'prop-types';
import Toolbar from '@material-ui/core/Toolbar';
import MenuFilter from './MenuFilter';
import Cookies from 'universal-cookie';
import Router from 'next/router';
import Link from 'next/link';

const cookies = new Cookies();

const SearchComp = ({ userData, hobbies }) => {
  const [myHobbies, setMyHobbies] = useState(hobbies);
  const [suggest, setSuggest] = useState(userData);
  const [messageLike, setMessageLike] = useState('');
  const upperCase = (string) => {
    if (string) return string.charAt(0).toUpperCase() + string.slice(1);
  };
  const [cards, setCards] = React.useState(() => {
    let tmp = [];
    for (let i of suggest)
      tmp.push([
        upperCase(i.firstName) + ',',
        i.age,
        i.name,
        i.geo[2],
        'url(' + i.images[0] + ')',
        i.score
      ]);
    return tmp;
  });
  const toProfil = (i) => async () => {
    const path = `/profil/${i[2]}`;
    await Router.push(path);
  };
  const Card = ({ zIndex = 0, children, ImageV, i }) => (
    <Link href={'profil?pseudo=' + i[2]} as={'/profil/' + i[2]}>
      <div className="containerCardSearch" key={i}>
        <div
          className="cardStyleSearch"
          style={{
            backgroundImage: ImageV,
            zIndex
          }}
          key={i[4]}
        >
          {children}
        </div>
        <div className="descriptionPeopleSearch">{i[0] + ' ' + i[1]}</div>
      </div>
    </Link>
  );
  const Card3 = ({ zIndex = 0, children }) => (
    <div className="containerCard">
      <div id="cardStyle3Search">{children}</div>
    </div>
  );
  return (
    <React.Fragment>
      <div id="searchContainer">
        <Toolbar id="ToolFilterSearch">
          <MenuFilter
            suggest={suggest}
            setSuggest={setSuggest}
            setCards={setCards}
            myHobbies={myHobbies}
          />
        </Toolbar>
        <div id="deckLists">
          {cards.length > 0 && (
            <div id="wrapperStyleSearch">
              {cards.map((i) => (
                <Card ImageV={i[4]} i={i} key={i[4]}></Card>
              ))}
            </div>
          )}
          {cards.length < 1 && (
            <Card3 zIndex={-1}>There is no matching profile</Card3>
          )}
        </div>
      </div>
      )}
      {cards.length < 1 && (
        <Card3 zIndex={-1}>There is no matching profile</Card3>
      )}
    </React.Fragment>
  );
};

SearchComp.getInitialProps = async () => {};

export default SearchComp;
