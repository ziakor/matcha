import React from 'react';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import Menu from '@material-ui/core/Menu';
import TuneIcon from '@material-ui/icons/Tune';
import Slider from '@material-ui/core/Slider';
import Input from '@material-ui/core/Input';
import Select from 'react-select';
import AddLocationIcon from '@material-ui/icons/AddLocation';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import ArrowDropUpIcon from '@material-ui/icons/ArrowDropUp';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';

const useStyles = makeStyles({
  root: {
    width: 250
  },
  input: {
    width: 42
  }
});
const MenuFilter = ({ t, suggest, setSuggest, setCards, myHobbies }) => {
  const classes = useStyles();
  const [anchorPage, setAnchorPage] = React.useState(null);
  const [AgeValue, setAgeValue] = React.useState([18, 100]);
  const [PopValue, setPopValue] = React.useState([1, 100]);
  const [LocValue, setLocValue] = React.useState(30);
  const [TagsValue, setTagsValue] = React.useState([]);

  const upperCase = (string) => {
    if (string) return string.charAt(0).toUpperCase() + string.slice(1);
  };
  const open = Boolean(anchorPage);

  const getHobbies = (data) => {
    var optTab = [];
    for (let i = 0; i < data.length; i++) {
      optTab.push({ value: data[i], label: data[i], color: '#202020' });
    }
    return optTab;
  };
  const options = getHobbies(myHobbies);

  const newCards = (tab) => {
    let tmp = [];
    if (tab) {
      for (let j of tab) {
        tmp.push([
          upperCase(j.firstName) + ',',
          j.age,
          j.name,
          j.geo[2],
          'url(' + j.images[0] + ')'
        ]);
      }
    }
    return tmp;
  };
  const handleClose = () => {
    let tab = [];
    let firstVerif;
    let newTabTags = [];
    if (TagsValue !== null)
      for (let a = 0; a < TagsValue.length; a++)
        newTabTags.push(TagsValue[a].value);
    let secondVerif = true;
    for (let i of suggest) {
      let count = 0;
      if (
        i.distance >= 0 &&
        i.distance <= LocValue &&
        (i.reputation >= PopValue[0] && i.reputation <= PopValue[1]) &&
        (i.age >= AgeValue[0] && i.age <= AgeValue[1])
      ) {
        firstVerif = true;
      } else firstVerif = false;
      if (newTabTags && newTabTags.length > 0) {
        for (let j of newTabTags) {
          for (let k of i.tags) {
            if (k === j) count++;
          }
        }
        if (count === newTabTags.length) secondVerif = true;
        else secondVerif = false;
        if (firstVerif === true && secondVerif === true) tab.push(i);
      } else {
        if (firstVerif == true) tab.push(i);
      }
    }
    setSuggest(tab);
    const newCardsTab = newCards(tab);
    setCards(newCardsTab);
    setAnchorPage(null);
  };

  const handleChangeAge = (event, newValue) => {
    if (newValue[0] < 18) setAgeValue([18, newValue[1]]);
    else setAgeValue(newValue);
  };
  const handleChangePop = (event, newValue) => {
    setPopValue(newValue);
  };
  const handleChangeLoc = (event, newValue) => {
    setLocValue(newValue);
  };
  const handleInputChangeLoc = (event) => {
    setLocValue(event.target.value === '' ? '' : Number(event.target.value));
  };
  const handleBlurLoc = () => {
    if (LocValue < 0) {
      setLocValue(0);
    }
  };
  const sortLoc = (value) => (event) => {
    let sortTab = suggest;
    let tmp;
    if (value === 0) {
      let i = 0;
      while (i < sortTab.length - 1) {
        if (sortTab[i].distance > sortTab[i + 1].distance) {
          tmp = sortTab[i];
          sortTab[i] = sortTab[i + 1];
          sortTab[i + 1] = tmp;
          i = 0;
        } else i++;
      }
    } else if (value === 1) {
      let j = 0;
      while (j < sortTab.length - 1) {
        if (sortTab[j].distance < sortTab[j + 1].distance) {
          tmp = sortTab[j];
          sortTab[j] = sortTab[j + 1];
          sortTab[j + 1] = tmp;
          j = 0;
        } else j++;
      }
    }
    setSuggest(sortTab);
    setCards(newCards(sortTab));
    setAnchorPage(null);
  };
  const sortPop = (value) => (event) => {
    let sortTab = suggest;
    let tmp;
    if (value === 0) {
      let i = 0;
      while (i < sortTab.length - 1) {
        if (sortTab[i].reputation > sortTab[i + 1].reputation) {
          tmp = sortTab[i];
          sortTab[i] = sortTab[i + 1];
          sortTab[i + 1] = tmp;
          i = 0;
        } else i++;
      }
    } else if (value === 1) {
      let j = 0;
      while (j < sortTab.length - 1) {
        if (sortTab[j].reputation < sortTab[j + 1].reputation) {
          tmp = sortTab[j];
          sortTab[j] = sortTab[j + 1];
          sortTab[j + 1] = tmp;
          j = 0;
        } else j++;
      }
    }
    setSuggest(sortTab);
    setCards(newCards(sortTab));
    setAnchorPage(null);
  };
  const sortAge = (value) => (event) => {
    let sortTab = suggest;
    let tmp;
    if (value === 0) {
      let i = 0;
      while (i < sortTab.length - 1) {
        if (sortTab[i].age > sortTab[i + 1].age) {
          tmp = sortTab[i];
          sortTab[i] = sortTab[i + 1];
          sortTab[i + 1] = tmp;
          i = 0;
        } else i++;
      }
    } else if (value === 1) {
      let j = 0;
      while (j < sortTab.length - 1) {
        if (sortTab[j].age < sortTab[j + 1].age) {
          tmp = sortTab[j];
          sortTab[j] = sortTab[j + 1];
          sortTab[j + 1] = tmp;
          j = 0;
        } else j++;
      }
    }
    setSuggest(sortTab);
    setCards(newCards(sortTab));
    setAnchorPage(null);
  };
  const sortTags = (value) => (event) => {
    let sortTab = suggest;
    let tmp;
    if (value === 0) {
      let i = 0;
      while (i < sortTab.length - 1) {
        if (sortTab[i].nbComTags > sortTab[i + 1].nbComTags) {
          tmp = sortTab[i];
          sortTab[i] = sortTab[i + 1];
          sortTab[i + 1] = tmp;
          i = 0;
        } else i++;
      }
    } else if (value === 1) {
      let j = 0;
      while (j < sortTab.length - 1) {
        if (sortTab[j].nbComTags < sortTab[j + 1].nbComTags) {
          tmp = sortTab[j];
          sortTab[j] = sortTab[j + 1];
          sortTab[j + 1] = tmp;
          j = 0;
        } else j++;
      }
    }
    setSuggest(sortTab);
    setCards(newCards(sortTab));
    setAnchorPage(null);
  };

  const handleTags = (values) => {
    setTagsValue((oldValues) => {
      return values;
    });
  };
  const handleMenu = (event) => {
    setAnchorPage(event.currentTarget);
  };
  return (
    <React.Fragment>
      <div id="filter-big-div">
        <IconButton
          id="MenuFilter"
          aria-label="Filter List"
          aria-haspopup="true"
          onClick={handleMenu}
          color="primary"
        >
          <TuneIcon color="primary" />
        </IconButton>
        <Menu
          id="menu-page"
          anchorEl={anchorPage}
          anchorOrigin={{
            vertical: 'top',
            horizontal: 'right'
          }}
          keepMounted
          transformOrigin={{
            vertical: 'top',
            horizontal: 'right'
          }}
          open={open}
          onClose={handleClose}
        >
          <div id="localSlider">
            <Typography id="input-slider" gutterBottom>
              <IconButton aria-label="delete" size="small" onClick={sortLoc(0)}>
                <ArrowDropUpIcon fontSize="inherit" />
              </IconButton>
              Localisation
              <IconButton aria-label="delete" size="small" onClick={sortLoc(1)}>
                <ArrowDropDownIcon fontSize="inherit" />
              </IconButton>
            </Typography>
            <Grid container spacing={2} alignItems="center">
              <Grid item>
                <AddLocationIcon
                  style={{ fontSize: 30, color: 'rgb(242, 16,90)' }}
                />
              </Grid>
              <Grid item xs>
                <Slider
                  value={typeof LocValue === 'number' ? LocValue : 0}
                  onChange={handleChangeLoc}
                  aria-labelledby="input-slider"
                />
              </Grid>
              <Grid item>
                <Input
                  className={classes.input}
                  value={LocValue}
                  margin="dense"
                  onChange={handleInputChangeLoc}
                  onBlur={handleBlurLoc}
                  inputProps={{
                    step: 10,
                    min: 0,
                    type: 'number',
                    'aria-labelledby': 'input-slider'
                  }}
                />
              </Grid>
            </Grid>
          </div>
          <div id="popSlider">
            <Typography id="popularity-slider" gutterBottom>
              <IconButton aria-label="delete" size="small" onClick={sortPop(0)}>
                <ArrowDropUpIcon fontSize="inherit" />
              </IconButton>
              Sort by popularity
              <IconButton aria-label="delete" size="small" onClick={sortPop(1)}>
                <ArrowDropDownIcon fontSize="inherit" />
              </IconButton>
            </Typography>
            <Slider
              value={PopValue}
              onChange={handleChangePop}
              valueLabelDisplay="auto"
              aria-labelledby="pop-slider"
              // getAriaValueText={valuetext}
              min={1}
              max={100}
            />
          </div>
          <div id="ageSlider">
            <Typography id="age-slider" gutterBottom>
              <IconButton aria-label="delete" size="small" onClick={sortAge(0)}>
                <ArrowDropUpIcon fontSize="inherit" />
              </IconButton>
              Sort by age
              <IconButton aria-label="delete" size="small" onClick={sortAge(1)}>
                <ArrowDropDownIcon fontSize="inherit" />
              </IconButton>
            </Typography>
            <Slider
              value={AgeValue}
              onChange={handleChangeAge}
              valueLabelDisplay="auto"
              aria-labelledby="range-slider"
              // getAriaValueText={valuetext}
              min={18}
              max={100}
            />
          </div>
          <div id="tagsSlider">
            <Typography id="age-slider" gutterBottom>
              <IconButton
                aria-label="delete"
                size="small"
                onClick={sortTags(0)}
              >
                <ArrowDropUpIcon fontSize="inherit" />
              </IconButton>
              Tags
              <IconButton
                aria-label="delete"
                size="small"
                onClick={sortTags(1)}
              >
                <ArrowDropDownIcon fontSize="inherit" />
              </IconButton>
            </Typography>
            <Select
              label="Tags"
              id="selectFilter"
              className="firstInput colorLabel"
              defaultValue={TagsValue}
              options={options}
              onChange={handleTags}
              isMulti
              SelectProps={{
                isCreatable: false,
                msgNoOptionsAvailable: 'No tags available',
                msgNoOptionsMatchFilter: 'no tags'
              }}
            />
          </div>
        </Menu>
      </div>
    </React.Fragment>
  );
};

MenuFilter.getInitialProps = async () => {};

export default MenuFilter;
