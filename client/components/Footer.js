import React, { useEffect, useState } from 'react';
import CookieIndicator from './CookieIndicator';
import Cookies from 'universal-cookie';

const cookie = new Cookies();
const Footer = () => {
  const [anchorIndicator, setAnchorIndicator] = useState(false);
  const [mount, setMount] = useState(true);
  const handleCookies = () => {
    setAnchorIndicator(false);
    cookie.set('acceptCookie', 'true', { path: '/' });
  };
  useEffect(() => {
    if (mount) {
      if (cookie.get('acceptCookie') === undefined) {
        setAnchorIndicator(true);
      }
      setMount(false);
    }
  });
  return (
    <footer id="salsifi">
      {anchorIndicator && (
        <CookieIndicator setAnchorIndicator={handleCookies} />
      )}
    </footer>
  );
};

export default Footer;
