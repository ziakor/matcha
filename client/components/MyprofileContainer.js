import React, { useState } from 'react';
import ListAltOutlinedIcon from '@material-ui/icons/ListAltOutlined';
import Avatar from '@material-ui/core/Avatar';
import Grid from '@material-ui/core/Grid';
import Divider from '@material-ui/core/Divider';
import Modal from '@material-ui/core/Modal';
import Cookies from 'universal-cookie';
import Link from 'next/link';
import {
  CarouselProvider,
  Slider,
  Slide,
  ButtonBack,
  ButtonNext
} from 'pure-react-carousel';
import 'pure-react-carousel/dist/react-carousel.es.css';

const cookies = new Cookies();

const MyprofileContainer = ({ userData }) => {
  const [history, setHistory] = useState(false);
  const [data, setData] = useState([]);

  let finalImages = [];
  for (let j = 0; j < userData.images.length; j++) {
    if (userData.images[j] !== '' && userData.images[j] !== undefined) {
      finalImages.push(userData.images[j]);
    }
  }
  userData.images = finalImages;

  const MultipleImage = () => {
    if (userData.images.length > 1) {
      return (
        <CarouselProvider
          naturalSlideWidth={2560}
          className="myprofileCarousel"
          naturalSlideHeight={1440}
          totalSlides={userData.images.length}
          visibleSlides={1}
          infinite={'true'}
        >
          <Slider>
            {userData.images.map((value, index) => {
              return (
                <Slide index={index} key={`${value}-${index}`}>
                  <div>
                    <img src={value} alt="user" className="image3" />
                  </div>
                </Slide>
              );
            })}
          </Slider>
        </CarouselProvider>
      );
    } else
      return (
        <CarouselProvider
          naturalSlideWidth={2560}
          className="myprofileCarousel"
          naturalSlideHeight={1440}
          totalSlides={userData.images.length}
          visibleSlides={1}
          infinite={'true'}
        >
          <Slider>
            {userData.images.map((value, index) => {
              return (
                <Slide index={index} key={`${value}-${index}`}>
                  <div>
                    <img src={value} alt="user" className="image3" />
                  </div>
                </Slide>
              );
            })}
          </Slider>
        </CarouselProvider>
      );
  };

  const handleChangeHistory = async () => {
    try {
      const response = await fetch('http://localhost:5000/myprofil/history', {
        method: 'GET',
        headers: {
          authorization: cookies.get('tokenCookie')
        }
      });

      if (response.status === 200) {
        const body = await response.json();
        setData(body.data);
        setHistory((oldvalues) => {
          return !oldvalues;
        });
      }
    } catch (e) {
      console.log(e);
    }
  };
  const displaySexualOrientation = () => {
    console.log(userData.genre, userData.target);
    if (userData.genre === "H") {
      if (userData.target === "H")
        return "Woman"
      else if (userData.target === "F")
        return "Man"
      else
        return "Both"
    }
    else {
      if (userData.target === "H")
        return "Man"
      else if (userData.target === "F")
        return "Woman"
      else
        return "Both"
    }
  }
  return (
    <Grid item xs={12} md={9} xl={10} id="myProfileContainer">
      <button
        title={'history List'}
        id="historyIcon"
        onClick={handleChangeHistory}
      >
        <ListAltOutlinedIcon />
      </button>
      <Modal
        aria-labelledby="modal-history-list"
        aria-describedby="modal-history-list2"
        open={history}
        onClose={handleChangeHistory}
      >
        <div id="historyContainer">
          <Divider id="dividerHistory" />
          <div id="contentHistory">
            {data.map((value, index) => {
              return (
                <div
                  className="notifItem"
                  key={`${value[0]}${value[1]}${value[2]}-${index}`}
                >
                  <Link
                    href={`/profil?pseudo=${value[0]}`}
                    as={`/profil/${value[0]}`}
                  >
                    <a id="linksMyskurt">{value[0]}</a>
                  </Link>{' '}
                  <span className="notifRelation">{value[1]}</span> {value[2]}
                </div>
              );
            })}
          </div>
        </div>
      </Modal>
      <Grid item xs={7} md={6} xl={4} id="infosMyProfile">
        <Grid item xl={12}>
          <div id="avatarContainerProfile">
            {userData.images[0] !== '' && (
              <Avatar
                alt={'user'}
                sizes="80px"
                id="avatarProfile"
                src={`${userData.images[0]}`}
              />
            )}
          </div>
        </Grid>
        <Grid item xl={12} id="pseudoContainer">
          <div id="tipsPseudo">Pseudo:</div>
          <div id="pseudoContainerProfile">
            <h1 className="h1Profile">{userData.name}</h1>
          </div>
          <Divider />
        </Grid>
        <Grid item xl={12}>
          <div id="nameContainerProfile">
            <h1 className="h1Profile">
              {userData.firstName} {userData.lastName}
            </h1>
          </div>
        </Grid>
        <Grid item xl={12} id="genreContainer">
          <div id="tipsGenre">Gender:</div>
          <div id="genderContainerProfile">
            <h1 className="h1Profile">{userData.genre}</h1>
          </div>
          <Divider />
        </Grid>
        <Grid item xl={12} id="geoContainer">
          <div id="tipsGeo">Town:</div>
          <div id="cityContainerProfile">
            <h1 className="h1Profile">{userData.geo[2]}</h1>
          </div>
          <Divider />
        </Grid>

        <Grid item xl={12} id="targetContainer">
          <div id="tipsTarget">Interested by:</div>
          <div id="sexualContainerProfile">
            <h1 className="h1Profile h1Target">{displaySexualOrientation()}</h1>
          </div>
          <div></div>
          <Divider />
        </Grid>
        <Divider className="dividerProfil" />
        <Grid item xl={12} id="bioContainer">
          <h2 id="titleBio">Bio:</h2>
          <div id="bioContainerProfile">
            <span id="bioSpan">{userData.bio}</span>
          </div>
          <div></div>
        </Grid>
        <Divider className="dividerProfil" />
        <Grid item xl={12} id="tagsContainer">
          <div id="tagsContainerProfile">
            <h3 id="titleTags">Tags:</h3>
            <div id="tagsCont">
              {userData.tags.map((value, index) => {
                if (index < userData.tags.length - 1) {
                  value = `${value}, `;
                }
                return (
                  <span key={`${value}${index}`}>
                    <span className="hash">#</span>
                    {value}
                  </span>
                );
              })}
            </div>
          </div>
          <div></div>
        </Grid>
      </Grid>
      <MultipleImage />
    </Grid>
  );
};

export default MyprofileContainer;
