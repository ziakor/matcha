/* eslint-disable jsx-a11y/anchor-is-valid */
import React from 'react';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Link from 'next/link';
import Grid from '@material-ui/core/Grid';
import Router from 'next/router';
import {
  FormControl,
  InputLabel,
  OutlinedInput,
  Input,
  ButtonGroup
} from '@material-ui/core';
import Cookies from 'universal-cookie';

const SigninContainer = () => {
  const [pseudoMailCheck, setPseudoMailCheck] = React.useState(true);
  const [passwordCheck, setPasswordCheck] = React.useState(true);

  const [pseudoMail, setPseudoMail] = React.useState('');
  const [password, setPassword] = React.useState('');
  const [message, setMessage] = React.useState('');

  const [isLogguedIn, SetIsLogguedIn] = React.useState('');
  const [isNotConfirmed, SetIsNotConfirmed] = React.useState('');

  const cookies = new Cookies();
  const [tokenState, setTokenState] = React.useState(
    cookies.get('tokenCookie')
  );

  const handleCheckPseudoMail = (event) => {
    const regexmail = /\S+@\S+\.\S+$/;
    const regexpseudo = /^[a-zA-Z]{4,15}$/;

    if (
      regexmail.test(event.target.value) ||
      regexpseudo.test(event.target.value)
    ) {
      setPseudoMailCheck(true);
      setPseudoMail(event.target.value);
    } else {
      setPseudoMailCheck(false);
    }
  };

  const handleCheckPassword = (event) => {
    const regex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/;
    if (!regex.test(event.target.value)) {
      setPasswordCheck(false);
    } else {
      setPasswordCheck(true);
      setPassword(event.target.value);
    }
  };

  const submitData = async () => {
    const data = JSON.stringify({
      mail: pseudoMail,
      password
    });
    try {
      const response = await fetch('http://localhost:5000/user/signin', {
        method: 'POST',
        body: data,
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json'
        }
      });
      const body = await response.json();
      if (response.status === 200) {
        SetIsLogguedIn(true);
        SetIsNotConfirmed(true);
        cookies.set('tokenCookie', body.token, { path: '/' });
        await Router.push('/match');
      } else if (response.status === 402) {
        SetIsLogguedIn(false);
        SetIsNotConfirmed(false);
      } else {
        SetIsLogguedIn(false);
      }
      return body;
    } catch (error) {
      return [].push(error);
    }
  };

  return (
    <React.Fragment>
      <div id="BigContSignin" />
      <div id="signinFormContainer">
        <div id="connexionWordForm">
          <Typography id="connexionWord" align="center" variant="caption">
            Connection
          </Typography>
        </div>
        {isNotConfirmed === false && (
          <h2 className="errorMessage">You have to confirm your account</h2>
        )}
        {isLogguedIn === false && isNotConfirmed !== false && (
          <h2 className="errorMessage">Invalid name or password</h2>
        )}

        <form id="signinForm">
          <Grid container item xs={12} justify="center" alignItems="center">
            <FormControl
              className="signinInputForm"
              required
              error={pseudoMailCheck === false}
            >
              <InputLabel id="InputLabelConnexion">Pseudo</InputLabel>
              <Input
                id="pseudomailInput"
                autoComplete="off"
                type="text"
                onBlur={handleCheckPseudoMail}
              />
            </FormControl>
          </Grid>
          <Grid item xs={12}>
            <FormControl
              className="signinInputForm"
              required
              error={passwordCheck === false}
            >
              <InputLabel id="InputLabelConnexion">Password</InputLabel>
              <Input
                id="passwordInput"
                type="password"
                autoComplete="off"
                onBlur={handleCheckPassword}
              />
            </FormControl>
          </Grid>
          <Grid item xs={12}>
            <Link href="/ResetPass">
              <a id="SigninForgot" align="center">
                <label style={{ cursor: 'pointer' }}>Forgot password?</label>
              </a>
            </Link>
          </Grid>
          <Button
            variant="contained"
            id="buttonConnexion"
            className="buttonHome"
            onClick={submitData}
          >
            Connection
          </Button>
          {/* <Button
            className="buttonHome"
            onClick={checkLogguedIn}
          >
            Testto
          </Button> */}
        </form>
      </div>
    </React.Fragment>
  );
};

SigninContainer.getInitialProps = async () => { };

export default SigninContainer;
