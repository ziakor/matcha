import Router from 'next/router';
import Cookies from 'universal-cookie';
import fetch from 'isomorphic-unfetch';

const cookies = new Cookies();

const serverUrl = 'http://localhost:5000';

module.exports = {
  handleAuthSSR: async (ctx) => {
    let token;
    if (ctx.req && ctx.req.headers.cookie) {
      token = ctx.req.headers.cookie.replace(
        /(?:(?:^|.*;\s*)tokenCookie\s*=\s*([^;]*).*$)|^.*$/,
        '$1'
      );
    } else {
      console.log('AUTH FRONT');
      token = cookies.get('tokenCookie');
    }
    if (token === undefined) token = '';
    try {
      const response = await fetch(serverUrl + '/user/checkAuth', {
        credentials: 'include',
        headers: {
          authorization: token
        }
      });
      if (response.status !== 200) {
        throw new Error('asd');
      }
      if (
        ctx.pathname === '/' ||
        ctx.pathname === '/signup' ||
        ctx.pathname === '/signin'
      ) {
        if (ctx.res) {
          ctx.res.writeHead(302, {
            Location: '/match'
          });
          ctx.res.end();
        } else {
          await Router.push('/match');
        }
      }
    } catch (err) {
      if (
        ctx.pathname !== '/' &&
        ctx.pathname !== '/signup' &&
        ctx.pathname !== '/signin' &&
        ctx.pathname !== '/ResetPass'
      ) {
        if (ctx.res) {
          ctx.res.writeHead(302, {
            Location: '/'
          });
          ctx.res.end();
        } else {
          Router.push('/');
        }
      }
    }
  },
  userInfo: async (ctx) => {
    let token = '';
    if (ctx.req && ctx.req.headers.cookie) {
      token = ctx.req.headers.cookie.replace(
        /(?:(?:^|.*;\s*)tokenCookie\s*=\s*([^;]*).*$)|^.*$/,
        '$1'
      );
    } else {
      token = cookies.get('tokenCookie');
    }
    if (token === undefined) token = '';
    try {
      const response = await fetch(serverUrl + '/profil/myinfos', {
        credentials: 'include',
        headers: {
          authorization: token
        }
      });
      if (response.status !== 200) {
        return null;
      }
      return await response.json();
    } catch (e) {
      console.log(e);
      return null;
    }
  },
  userMatchs: async (ctx) => {
    let token = '';
    if (ctx.req && ctx.req.headers.cookie) {
      token = ctx.req.headers.cookie.replace(
        /(?:(?:^|.*;\s*)tokenCookie\s*=\s*([^;]*).*$)|^.*$/,
        '$1'
      );
    } else {
      token = cookies.get('tokenCookie');
    }
    if (token === undefined) token = '';
    try {
      const response = await fetch(serverUrl + '/user/matchs', {
        credentials: 'include',
        headers: {
          authorization: token
        }
      });
      if (response.status !== 200) {
        return null;
      }
      return await response.json();
    } catch (e) {
      console.log(e);
      return null;
    }
  },
  getOtherInfo: async (ctx) => {
    let token = '';

    if (ctx.req && ctx.req.headers.cookie) {
      token = ctx.req.headers.cookie.replace(
        /(?:(?:^|.*;\s*)tokenCookie\s*=\s*([^;]*).*$)|^.*$/,
        '$1'
      );
    } else {
      token = cookies.get('tokenCookie');
    }
    if (token === undefined) token = '';

    try {
      const response = await fetch(serverUrl + `/profil/${ctx.query.pseudo}`, {
        credentials: 'include',
        headers: {
          authorization: token
        }
      });
      if (response.status !== 200) {
        if (response.status === 450) {
          if (ctx.res) {
            ctx.res.writeHead(302, {
              Location: '/myprofile'
            });
            ctx.res.end();
          } else {
            Router.push('/myprofile');
          }
          return null;
        }
        return null;
      }

      return await response.json();
    } catch (e) {
      console.log('ERROR REQUETE SITEBACK');
      return null;
    }
  },
  getNotifs: async (ctx) => {
    let token = '';

    if (ctx.req && ctx.req.headers.cookie) {
      token = ctx.req.headers.cookie.replace(
        /(?:(?:^|.*;\s*)tokenCookie\s*=\s*([^;]*).*$)|^.*$/,
        '$1'
      );
    } else {
      token = cookies.get('tokenCookie');
    }
    if (token === undefined) token = '';
    try {
      const response = await fetch(serverUrl + `/notifications/get`, {
        credentials: 'include',
        headers: {
          authorization: token
        }
      });
      if (response.status !== 200) return null;
      const body = await response.json();
      return body;
    } catch (e) {
      return null;
    }
  },
  getMatchedUser: async (ctx) => {
    let token = '';
    if (ctx.req && ctx.req.headers.cookie) {
      token = ctx.req.headers.cookie.replace(
        /(?:(?:^|.*;\s*)tokenCookie\s*=\s*([^;]*).*$)|^.*$/,
        '$1'
      );
    } else {
      token = cookies.get('tokenCookie');
    }
    if (token === undefined) token = '';
    try {
      const response = await fetch(serverUrl + '/chat/getMatchUser', {
        credentials: 'include',
        headers: {
          authorization: token
        }
      });
      if (response.status === 200) return response.json();
    } catch (e) {
      return null;
    }
  }
};
