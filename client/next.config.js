// next.config.js
const withCSS = require('@zeit/next-css');

function HACK_removeMinimizeOptionFromCssLoaders(config) {
  config.module.rules.forEach((rule) => {
    if (Array.isArray(rule.use)) {
      rule.use.forEach((u) => {
        if (u.loader === 'css-loader' && u.options) {
          delete u.options.minimize;
        }
      });
    }
  });
}

module.exports = withCSS({
  webpack(config) {
    // HACK_removeMinimizeOptionFromCssLoaders(config);
    config.module.rules.push({
      test: /\.(eot|woff|woff2|ttf|svg|png|jpg|jpeg|gif)$/,
      use: {
        loader: 'url-loader',
        options: {
          limit: 10000000,
          name: '[name].[ext]'
        }
      }
    });
    return config;
  }
});
