import React from 'react';
import PropTypes from 'prop-types';

import Customhead from '../components/Customhead';
import Footer from '../components/Footer';
import AppBar from '../components/Appbar';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import CarouselBackground from '../components/CarouselBackground';
import Confirmation from '../components/Confirmation';

import SigninForm from '../components/SigninForm';
import auth from '../utils/auth';

const Signin = ({ auth }) => (
  <React.Fragment>
    <Customhead title="Signin" />
    <main>
      <AppBar auth={auth} />
      <Confirmation />
      <CarouselBackground />
      <Grid
        container
        id="BigSigninDiv"
        justify="center"
        xl={12}
        alignItems="center"
      >
        <Grid item xl={6} align="center">
          <SigninForm />
        </Grid>
      </Grid>
    </main>
    {/* <Footer /> */}
  </React.Fragment>
);

Signin.getInitialProps = async (ctx) => {
  await auth.handleAuthSSR(ctx);
  return { auth: false };
};

export default Signin;
