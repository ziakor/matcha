import React from 'react';
import PropTypes from 'prop-types';

import Customhead from '../components/Customhead';
import Footer from '../components/Footer';
import AppBar from '../components/Appbar';
import Grid from '@material-ui/core/Grid';
import MessComp from '../components/MessComp';
import MatchBackground from '../components/MatchBackground';
import '../css/main.css';
import auth from '../utils/auth';

const Messenger = ({ myData, notifs, auth, userData }) => {
  return (
    <React.Fragment>
      <Customhead title="Messenger" />
      <main>
        <AppBar notifs={notifs} auth={auth} />
        <MatchBackground />
        <Grid
          id="matchContainer"
          container
          justify="center"
          xl={12}
          alignItems="center"
        >
          <Grid item xl={6} align="center">
            <MessComp myData={myData} userData={userData} />
          </Grid>
        </Grid>
      </main>
      <Footer />
    </React.Fragment>
  );
};

Messenger.getInitialProps = async (ctx) => {
  await auth.handleAuthSSR(ctx);
  let data = await auth.userInfo(ctx);
  if (data === null) {
    data = { data: { images: [''] }, name: 'Unknown' };
  } else {
    if (data.data.images[0] !== undefined && data.data.images[0] !== '') {
      data.data.images[0] = `/static/dbImages/${data.data.images[0]}`;
      data.data.images.length = 1;
    }
  }

  // const userData = [{ name: "ziakor", image: "bite.jpg" }, { name: "awdawdawd", image: "bite.jpg" }, { name: "ziakorr", image: "bite.jpg" }]
  // const message = {myMessage: ["asdasdsa", "0537537" , "asdasd"], hisMessage: ["awdawd", "awdawd"]}
  //http://localhost:5000/chat/get/:pseudo

  let notif = await auth.getNotifs(ctx);

  if (notif === null) {
    notif = { data: [] };
  }
  let userMatch = await auth.getMatchedUser(ctx);

  if (userMatch === null) userMatch = { data: [] };
  else {
    if (userMatch.data) {
      for (let i = 0; i < userMatch.data.length; i++) {
        userMatch.data[i].image = `/static/dbImages/${userMatch.data[i].image}`;
      }
    } else {
      userMatch = { data: [] };
    }
  }

  return {
    myData: data.data,
    notifs: notif.data,
    auth: true,
    userData: userMatch.data
  };
};

export default Messenger;
