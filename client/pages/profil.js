import React, { useState } from 'react';
import PropTypes from 'prop-types';
import Customhead from '../components/Customhead';
import Footer from '../components/Footer';
import AppBar from '../components/Appbar';
import Grid from '@material-ui/core/Grid';
import auth from '../utils/auth';
import ProfileContainer from '../components/profileContainer';
import Error from 'next/error';
import '../css/main.css';

const Profil = ({ userData, statusCode, notifs, auth }) => {
  if (statusCode >= 400) return <Error statusCode={statusCode} />;

  return (
    <div id="settingsPage">
      <Customhead title="Index" />
      <main>
        <AppBar notifs={notifs} auth={auth} />
        <div id="cont">
          <Grid
            id="settingsContainer"
            container
            justify="center"
            xl={12}
            alignItems="center"
          >
            <ProfileContainer userData={userData} />
          </Grid>
        </div>
      </main>
      <Footer />
    </div>
  );
};

Profil.getInitialProps = async (ctx) => {
  await auth.handleAuthSSR(ctx);
  let data = await auth.getOtherInfo(ctx);
  let statusCode;
  if (data === null) {
    data = { data: '' };
    statusCode = 424;
  } else {
    if (data.data.connected !== '') {
      data.data.connected = new Date(parseInt(data.data.connected)).toString(
        'yyyy-MM-d-h-mm-ss'
      );
    }
    statusCode = 200;
    for (let i = 0; i < 5; i++) {
      if (data.data.images[i] !== '')
        data.data.images[i] = `/static/dbImages/${data.data.images[i]}`;
    }
  }
  let notif = await auth.getNotifs(ctx);

  if (notif === null) {
    notif = { data: [] };
  }

  return {
    userData: data.data,
    statusCode: statusCode,
    notifs: notif.data,
    auth: true
  };
};

export default Profil;
