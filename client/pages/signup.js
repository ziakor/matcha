import React from 'react';
import PropTypes from 'prop-types';

import Customhead from '../components/Customhead';
import Footer from '../components/Footer';
import AppBar from '../components/Appbar';
import Grid from '@material-ui/core/Grid';
import CarouselBackground from '../components/CarouselBackground';
import SignupContainer from '../components/SignupContainer';
import auth from '../utils/auth';

const Signup = ({ auth }) => (
  <React.Fragment>
    <Customhead title="Signup" />
    <main>
      <AppBar auth={auth} />
      <CarouselBackground />
      <Grid
        id="signupFormContainer"
        container
        justify="center"
        xl={12}
        alignItems="center"
      >
        <Grid
          item
          xs={12}
          sm={10}
          md={7}
          xl={4}
          align="center"
          id="formContainer"
        >
          <SignupContainer />
        </Grid>
      </Grid>
    </main>
    <Footer />
  </React.Fragment>
);

Signup.getInitialProps = async (ctx) => {
  await auth.handleAuthSSR(ctx);
  return { auth: false };
};

export default Signup;
