import React from 'react';

import Customhead from '../components/Customhead';
import Footer from '../components/Footer';
import AppBar from '../components/Appbar';
import Grid from '@material-ui/core/Grid';
import CarouselBackground from '../components/CarouselBackground';
import ResetPassForm from '../components/ResetPassForm';
import auth from '../utils/auth';
import '../css/main.css';

const ResetPass = ({ auth }) => {
  return (
    <React.Fragment>
      <Customhead title="Match" />
      <main>
        <AppBar auth={auth} />
        <CarouselBackground />
        <Grid
          id="matchContainer"
          container
          justify="center"
          xl={12}
          alignItems="center"
        >
          <Grid item xl={6} align="center">
            <ResetPassForm />
          </Grid>
        </Grid>
      </main>
      <Footer />
    </React.Fragment>
  );
};

ResetPass.getInitialProps = async (ctx) => {
  await auth.handleAuthSSR(ctx);
  return { auth: false };
};

export default ResetPass;
