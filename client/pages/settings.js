import React from 'react';
import PropTypes from 'prop-types';
import Customhead from '../components/Customhead';
import Footer from '../components/Footer';
import AppBar from '../components/Appbar';
import Grid from '@material-ui/core/Grid';
import auth from '../utils/auth';
import SettingsForm from '../components/SettingsForm';
import '../css/main.css';

const Settings = ({ userImages, userTags, notifs, auth }) => {
  return (
    <div id="settingsPage">
      <Customhead title="Settings" />
      <main>
        <AppBar notifs={notifs} auth={auth} />
        <div id="cont">
          <h1 id="titleSettingsForm"></h1>
          <Grid
            id="settingsContainer"
            container
            justify="center"
            xl={12}
            alignItems="center"
          >
            <SettingsForm userData={userImages} userTags={userTags} />
          </Grid>
        </div>
      </main>
      <Footer />
    </div>
  );
};

Settings.getInitialProps = async (ctx) => {
  await auth.handleAuthSSR(ctx);
  let data = await auth.userInfo(ctx);
  if (data === null) {
    data = { data: { images: ['', '', '', '', ''] }, tags: [] };
  } else {
    if (data.data.images[0] !== undefined) {
      for (let i = 0; i < 5; i++) {
        if (data.data.images[i] !== '')
          data.data.images[i] = `/static/dbImages/${data.data.images[i]}`;
      }
    }
  }
  let notif = await auth.getNotifs(ctx);

  if (notif === null) {
    notif = { data: [] };
  }

  return {
    namespacesRequired: ['settings'],
    userImages: data.data.images,
    userTags: data.data.tags,
    notifs: notif.data,
    auth: true
  };
};

export default Settings;
