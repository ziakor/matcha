import React from 'react';
import PropTypes from 'prop-types';

const Error = ({ statusCode, t }) => (
  <p>
    {statusCode !== null ? 'error with status' + '400' : 'Error without status'}
  </p>
);

Error.getInitialProps = async ({ res, err }) => {
  let statusCode = null;
  if (res) {
    ({ statusCode } = 400);
  } else if (err) {
    ({ statusCode } = 400);
  }
  return {
    statusCode
  };
};

Error.defaultProps = {
  statusCode: null
};

Error.propTypes = {
  statusCode: PropTypes.number
};

export default Error;
