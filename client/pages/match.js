import React from 'react';
import PropTypes from 'prop-types';

import Customhead from '../components/Customhead';
import Footer from '../components/Footer';
import AppBar from '../components/Appbar';
import Grid from '@material-ui/core/Grid';
import MatchSwipe from '../components/MatchSwipe';
import MatchBackground from '../components/MatchBackground';
import auth from '../utils/auth';
import '../css/main.css';

const Match = ({ userData, hobbies, notifs, auth }) => {
  return (
    <React.Fragment>
      <Customhead title="Match" />
      <main>
        <AppBar notifs={notifs} auth={auth} />
        <MatchBackground />
        <Grid
          id="matchContainer"
          container
          justify="center"
          xl={12}
          alignItems="center"
        >
          <Grid item xl={6} align="center">
            <MatchSwipe userData={userData} hobbies={hobbies} />
          </Grid>
        </Grid>
      </main>
      <Footer />
    </React.Fragment>
  );
};

Match.getInitialProps = async (ctx) => {
  await auth.handleAuthSSR(ctx);
  let data = await auth.userMatchs(ctx);
  const myHobbies = data.data[data.data.length - 1].MyHobbies;
  const myImg = data.data[data.data.length - 1].MyImages;
  if (myImg[0] === '' && ctx.res) {
    ctx.res.writeHead(302, {
      Location: '/settings'
    });
    ctx.res.end();
  }
  data.data.pop();
  let tab = [];
  for (let t = 0; t < data.data.length; t++) {
    if (data.data[t].images[0] !== undefined && data.data[t].images[0] !== '') {
      for (let i = 0; i < 5; i++) {
        if (data.data[t].images[i] !== '')
          data.data[t].images[i] = `/static/dbImages/${data.data[t].images[i]}`;
      }
      data.data[t].age = Math.floor(data.data[t].age);
      tab.push(data.data[t]);
    } else delete data.data[t];
  }
  data.data = tab;
  let notif = await auth.getNotifs(ctx);

  if (notif === null) {
    notif = { data: [] };
  }
  return {
    userData: data.data,
    hobbies: myHobbies,
    notifs: notif.data,
    auth: true
  };
};

export default Match;
