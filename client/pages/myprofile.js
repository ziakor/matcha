import React from 'react';
import Customhead from '../components/Customhead';
import Footer from '../components/Footer';
import AppBar from '../components/Appbar';
import Grid from '@material-ui/core/Grid';
import auth from '../utils/auth';
import MyprofileContainer from '../components/MyprofileContainer';
import Error from 'next/error';
import '../css/main.css';

const MyProfile = ({ userData, statusCode, notifs, auth }) => {
  if (statusCode !== 200) {
    // This line will show the default Error Page
    return <Error statusCode={statusCode} />;
  }
  return (
    <div id="settingsPage">
      <Customhead title="My profile" />
      <main>
        <AppBar notifs={notifs} auth={auth} />
        <div id="cont">
          <Grid
            id="settingsContainer"
            container
            justify="center"
            xl={12}
            alignItems="center"
          >
            <MyprofileContainer userData={userData} />
          </Grid>
        </div>
      </main>
      <Footer />
    </div>
  );
};

MyProfile.getInitialProps = async (ctx) => {
  await auth.handleAuthSSR(ctx);
  let data = await auth.userInfo(ctx);
  let statusCode;
  if (data === null) {
    data = { data: [''] };
    statusCode = 424;
  } else {
    statusCode = 200;
    for (let i = 0; i < 5; i++) {
      if (data.data.images[i] !== '')
        data.data.images[i] = `/static/dbImages/${data.data.images[i]}`;
    }
  }

  let notif = await auth.getNotifs(ctx);

  if (notif === null) {
    notif = { data: [] };
  }

  return {
    userData: data.data,
    statusCode: statusCode,
    notifs: notif.data,
    auth: true
  };
};

export default MyProfile;

//242 16 90
