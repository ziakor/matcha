import React from 'react';
import Customhead from '../components/Customhead';
import Footer from '../components/Footer';
import AppBar from '../components/Appbar';
import CarouselBackground from '../components/CarouselBackground';
import HomeButton from '../components/HomeButton';
import Confirmation from '../components/Confirmation';
import auth from '../utils/auth';
import '../css/main.css';

const Homepage = ({ auth }) => {
  return (
    <React.Fragment>
      <Customhead title="Homepage" id="testo" />
      <main>
        <AppBar title={'Index'} auth={auth} />
        <Confirmation />
        <HomeButton />
        <CarouselBackground />
      </main>
      <Footer />
    </React.Fragment>
  );
};

Homepage.getInitialProps = async (ctx) => {
  await auth.handleAuthSSR(ctx);
  return { auth: false };
};

export default Homepage;
