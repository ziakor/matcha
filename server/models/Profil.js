var bcrypt = require('bcrypt');
const config = require('../config/index');
const relation = require('./Relationship');
const securite = require('./Secu');
const Mail = require('./Mail');
const Notif = require('./NotificationsModel');
const Chat = require('../models/Chat');
module.exports = {
  getDataOtherProfil: async (driver, pseudo, id) => {
    //ADD CHECK IF PERSON BLOQUER
    const session = driver.session();
    let like = 0;
    let tags = [];
    try {
      const response = await session.run(
        'match(p:Person{name:{pseudo}}) return p',
        {
          pseudo: pseudo
        }
      );
      if (response.records[0] === undefined) return null;
      if (response.records[0].get(0).properties.uuid === id) return null;
      try {
        const response = await session.run(
          'match(p:Person{name: {pseudo}})-[r:LIKE]->(t:Tags) return t',
          { pseudo: pseudo }
        );
        if (response.records[0] === undefined) return null;
        for (value of response.records) {
          tags.push(value.get(0).properties.name);
        }

        try {
          const resultPromise = await session.run(
            'match(a:Person{uuid: {id}})-[r:LOVE]->(b:Person{name: {pseudo}}) return a',
            { id: id, pseudo: pseudo }
          );
          if (resultPromise.records[0] !== undefined) {
            like++;
          }
        } catch (e) {
          like = 0;
        }
        try {
          const resultPromise = await session.run(
            'match(a:Person{uuid: {id}})<-[r:LOVE]-(b:Person{name: {pseudo}}) return b',
            { id: id, pseudo: pseudo }
          );

          if (resultPromise.records[0] !== undefined) {
            like += 2;
          }
        } catch (e) {
          like = 0;
        }
      } catch (e) {
        return null;
      }

      delete response.records[0].get(0).properties.passwd;
      delete response.records[0].get(0).properties.mail;
      delete response.records[0].get(0).properties.created;
      delete response.records[0].get(0).properties.confirmation;
      delete response.records[0].get(0).properties.uuid;
      response.records[0].get(0).properties.tags = tags;
      response.records[0].get(0).properties.like = like;
      return response.records[0].get(0).properties;
    } catch (e) {
      return null;
    }
  },
  likeProfil: async (driver, pseudo, id, io) => {
    const session = driver.session();
    let ret = 0;
    try {
      const response = await session.run(
        'match(a:Person{uuid: {id}})-[r:LOVE]->(b:Person{name:{pseudo}}) return a,r,b',
        { id: id, pseudo: pseudo }
      );

      if (response.records[0] === undefined) {
        const response = await session.run(
          'match(a:Person{uuid: {id}}),(b:Person{name:{pseudo}}) SET b.reputation=(CASE ' +
          'WHEN (b.reputation  + 5) > 100 THEN 100' +
          ' ELSE (b.reputation + 5) END)' +
          'merge(a)-[r:LOVE]->(b)' +
          ' merge (a)-[r1:NOTIF{type: "LIKE", seen: false}]->(b)' +
          ' return b,a',
          { id: id, pseudo: pseudo }
        );
        const uuidTarget = response.records[0].get(0).properties;
        const uuidTarget2 = response.records[0].get(1).properties;
        if ((await Notif.checkBlock(driver, id, pseudo)) === false) {
          io.to(uuidTarget.uuid).emit('notif', {
            pseudo: uuidTarget2.name,
            type: 'LIKE'
          });
        }
        ret = 1;

        try {
          const check = await session.run(
            'match(a:Person{uuid: {id}})<-[r:LOVE]-(b:Person{name: {pseudo}}) return r',
            {
              id: id,
              pseudo: pseudo
            }
          );
          if (check.records[0] !== undefined) {
            await Chat.createChat(driver, pseudo, id);
          }
        } catch (e) { }
      } else {
        const response = await session.run(
          'match(a:Person{uuid: {id}})-[r:LOVE]->(b:Person{name: {pseudo}}) SET b.reputation=' +
          '(CASE WHEN (b.reputation - 5) < 0 THEN 0 ELSE (b.reputation - 5) END) DELETE r return b,a',
          {
            id: id,
            pseudo: pseudo
          }
        );
        const dislike = response.records[0].get(0).properties;
        const dislike2 = response.records[0].get(1).properties;
        if ((await Notif.checkBlock(driver, id, pseudo)) === false) {
          io.to(dislike.uuid).emit('notif', {
            pseudo: dislike2.name,
            type: 'DISLIKE'
          });
        }
        const d = await Chat.deleteRoom(driver, id, pseudo);
        await session.run(
          'match(a:Person{uuid: {id}})-[r:LIKE]->(b:Person{name: {pseudo}}) DETACH DELETE r',
          {
            id: id,
            pseudo: pseudo
          }
        );
        ret = 0;
      }
    } catch (e) {
      return -1;
    }
    return ret;
  },
  blockProfil: async (driver, pseudo, id) => {
    const session = driver.session();

    try {
      await session.run(
        'match(a:Person{uuid: {id}}),(b:Person{name:{pseudo}}) SET b.reputation=(CASE WHEN (b.reputation - 2) < 0 THEN' +
        ' 0 ELSE (b.reputation - 2) END) ' +
        ' MERGE(a)-[r:BLOCK]->(b) return a',

        {
          id: id,
          pseudo: pseudo
        }
      );
    } catch (e) {
      return false;
    }
    return true;
  },

  reportProfil: async (driver, pseudo, id) => {
    const session = driver.session();

    try {
      const response = await session.run(
        'match(a:Person{name: {pseudo}}),(b:Person{uuid:{id}}) SET a.reputation=(CASE WHEN (a.reputation  - 10)< 0 THEN 0' +
        ' ELSE (a.reputation - 10) END)' +
        ' merge (b)-[r:REPORT]->(a) return r, a',
        { pseudo: pseudo, id: id }
      );

      if (response.records[0] === undefined) return false;
      //SENDMAIL ET REDUIRE POINT DE REPUTATION
      Mail.sendMailReport(
        response.records[0].get(1).properties.mail,
        response.records[0].get(1).properties.name,
        'Report'
      );
      return true;
    } catch (e) {
      return false;
    }
  },
  visitProfile: async (driver, pseudo, id, io) => {
    const session = driver.session();

    try {
      const date = Date.now().toString();
      const response = await session.run(
        'match (a:Person{uuid : {id}}),(b:Person{name:{pseudo}})' +
        ' MERGE (a)-[r:VISIT]->(b) ON CREATE set r.date={date},b.reputation=(CASE WHEN (b.reputation + 1)>100 THEN 100 ' +
        'ELSE (b.reputation + 1) END)' +
        ' MERGE (a)-[r2:NOTIF{type:"VISIT", seen: false}]->(b)' +
        ' return r,a,b',
        { id: id, pseudo: pseudo, date: date }
      );
      const user1 = response.records[0].get(1).properties;
      const user2 = response.records[0].get(2).properties;
      if ((await Notif.checkBlock(driver, id, pseudo)) === false) {
        io.to(user2.uuid).emit('notif', {
          pseudo: user1.name,
          type: 'VISIT'
        });
      }
      return response.records[0] !== undefined;
    } catch (e) {
      return false;
    }
  }
};
