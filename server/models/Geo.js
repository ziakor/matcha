const config = require('../config/index');
const fetch = require('isomorphic-unfetch');

module.exports = {
  getLocalisation: async (req) => {
    const data = req.body.geo;
    let res = ['0', '0'];
    let ip = null;
    if (data[0] === 'true') {
      try {
        const response = await fetch(
          `https://eu1.locationiq.com/v1/reverse.php?key=${config.GO_API}&lat=${
            data[1]
          }&lon=${data[2]}&format=json`
        );
        const body = await response.json();
        res[0] = data[1].toString();
        res[1] = data[2].toString();
        res[2] = body.address.city === null ? '' : body.address.city;

        return res;
      } catch (e) {
        res = null;
      }
    } else {
      try {
        const response = await fetch(`https://freegeoip.app/json/`);
        const body = await response.json();
        res[0] = body.latitude.toString();
        res[1] = body.longitude.toString();

        res[2] = body.city === null ? '' : body.city;
        return res;
      } catch (e) {
        res = null;
      }

      return res;
    }
  },
  getDistance: (lat1, lon1, lat2, lon2) => {
    if (lat1 === lat2 && lon1 === lon2) return 0;
    else {
      let radlat1 = (Math.PI * lat1) / 180;
      let radlat2 = (Math.PI * lat2) / 180;
      let theta = lon1 - lon2;
      let radtheta = (Math.PI * theta) / 180;
      let dist =
        Math.sin(radlat1) * Math.sin(radlat2) +
        Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
      if (dist > 1) {
        dist = 1;
      }
      dist = Math.acos(dist);
      dist = (dist * 180) / Math.PI;
      dist = dist * 60 * 1.1515;
      dist = dist * 1.609344;
      return dist;
    }
  }
};
