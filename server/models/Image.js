const fs = require('fs');
const path = require('path');
const https = require('https');

module.exports = {
  deleteImage: function(data) {
    for (let index = 0; index < data.length; index++) {
      fs.unlink(
        `${__dirname}/../../client/static/dbImages/${data[index]}`,
        (err) => {}
      );
    }
  },
  saveImageToDisk: (url, Path) => {
    const file = fs.createWriteStream(Path);
    const request = https.get(url, (response) => {
      response.pipe(file);
    });
  }
};
