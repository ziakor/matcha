module.exports = {
  removeActiveNotifications: async (driver, id) => {
    const session = driver.session();
    try {
      const response = await session.run(
        'match(a:Person{uuid: {id}})<-[r:NOTIF]-(b:Person) DETACH DELETE r',
        {
          id: id
        }
      );
      return response.records[0];
    } catch (e) {
      return null;
    }
  },
  getNotifications: async (driver, id) => {
    const session = driver.session();
    try {
      const response = await session.run(
        'match(a:Person{uuid: {id}})<-[r:NOTIF]-(b:Person) where r.seen=false return a,r,b',
        {
          id: id
        }
      );
      if (response.records[0] === undefined) return [];
      else {
        let tab = [];
        for (let i = 0; i < response.records.length; i++) {
          tab.push([
            response.records[i].get(2).properties.name,
            response.records[i].get(1).properties.type
          ]);
        }
        return tab;
      }
    } catch (e) {
      return null;
    }
  },
  checkBlock: async (driver, id, pseudo) => {
    const session = driver.session();

    try {
      const response = await session.run(
        'match(a:Person{uuid:' +
        '{id}})<-[r:BLOCK]-' +
        '(b:Person{pseudo:{pseudo}}) return r',
        {
          id: id,
          pseudo: pseudo
        }
      );
      if (response.records[0] !== undefined) return true;
    } catch (e) {
      return true;
    }
    return false;
  }
};
