const Notif = require('./NotificationsModel');
module.exports = {
  createChat: async (driver, pseudo, id) => {
    const session = driver.session();

    try {
      const response = await session.run(
        'match(a:Person{name:{pseudo}}) return a.uuid',
        {
          pseudo: pseudo
        }
      );
      if (response.records[0] !== undefined) {
        const nameRoom = id + '+' + response.records[0].get(0);
        //Creer la room sur la db
        try {
          const response2 = await session.run(
            'match(a:Person{uuid:{id}})-[r:CHAT]->(c:Room)<-[r1:CHAT]-(b:Person{uuid:{secondId}})' +
            'WHERE c.name={first} OR c.name={second} return c',
            {
              id: id,
              nameRoom: nameRoom,
              secondId: response.records[0].get(0),
              first: id + '+' + response.records[0].get(0),
              second: response.records[0].get(0) + '+' + id
            }
          );
          if (response2.records[0] === undefined) {
            await session.run(
              'match(a:Person{uuid:{id}}),(b:Person{uuid:{secondId}})' +
              'CREATE(a)-[r:CHAT]->(c:Room{name:{nameRoom}, message1User:{id}, message2User:{secondId}, message1:[], message2:[]})<-[r1:CHAT]-(b)',
              {
                id: id,
                nameRoom: nameRoom,
                secondId: response.records[0].get(0),
                first: id + '+' + response.records[0].get(0),
                second: response.records[0].get(0) + '+' + id
              }
            );
          }
          return true;
        } catch (e) {
        }
      }
    } catch (e) {
    }
    return false;
  },
  addMessage: async (driver, id, pseudo, message, io) => {
    const session = driver.session();

    try {
      const response = await session.run(
        'match(a:Person{name:{pseudo}}) return a.uuid',
        {
          pseudo: pseudo
        }
      );

      if (response.records[0] !== undefined) {
        const date = Date.now().toString();

        try {
          const response2 = await session.run(
            'match (a:Person{uuid: {id}})-[r:CHAT]->(c:Room)<-[r1:CHAT]-' +
            '(b:Person{uuid:{secondId}}) WHERE c.name={first} OR c.name={second} ' +
            'SET c.message1=(CASE WHEN a.uuid=c.message1User THEN c.message1+{newMessage}+{date} ELSE c.message1 END),' +
            ' c.message2=(CASE WHEN a.uuid=c.message2User THEN c.message2+{newMessage}+{date} ELSE c.message2 END)' +
            ' CREATE(a)-[r2:NOTIF{seen:false, type:"MESSAGE"}]->(b) return a',
            {
              id: id,
              secondId: response.records[0].get(0),
              first: id + '+' + response.records[0].get(0),
              second: response.records[0].get(0) + '+' + id,
              pseudo: pseudo,
              newMessage: message,
              date: date
            }
          );
          //ADD NOTIF WEBSOCKET
          const rep = await Notif.checkBlock(driver, id, pseudo);
          if (rep === false) {
            await io.to(response.records[0].get(0)).emit('message', {
              pseudo: response2.records[0].get(0).properties.name,
              message: message
            });

            await io.to(response.records[0].get(0)).emit('notif', {
              pseudo: response2.records[0].get(0).properties.name,
              type: 'MESSAGE'
            });
          }
          return true;
        } catch (e) {
          return false;
        }
      }
    } catch (e) {
      return false;
    }
    return false;
  },
  getRoom: async (driver, id, pseudo) => {
    const session = driver.session();

    try {
      const response = await session.run(
        'match(a:Person{name:{pseudo}}) return a',
        {
          pseudo: pseudo
        }
      );
      if (response.records[0] !== undefined) {
        try {
          const first = id + '+' + response.records[0].get(0).properties.uuid;
          const second = response.records[0].get(0).properties.uuid + '+' + id;
          const response2 = await session.run(
            'match(a:Person{uuid: {id}})-[r:CHAT]->(c:Room)<-[r1:CHAT]' +
            '-(b:Person{name:{pseudo}}) WHERE c.name={first} OR c.name={second} return c,a',
            {
              id: id,
              pseudo: pseudo,
              secondId: response.records[0].get(0).properties.uuid,
              first: first,
              second: second
            }
          );

          let myMessage = [];
          let hisMessage = [];
          if (response2.records[0].get(0).properties.message1User === id) {
            myMessage = response2.records[0].get(0).properties.message1;
            hisMessage = response2.records[0].get(0).properties.message2;
          } else {
            hisMessage = response2.records[0].get(0).properties.message1;
            myMessage = response2.records[0].get(0).properties.message2;
          }
          let data = {
            connected: response2.records[0].get(1).properties.connected,
            pseudo: pseudo,
            image: response.records[0].get(0).properties.images[0]
          };
          data.myMessage = myMessage;
          data.hisMessage = hisMessage;

          return data;
        } catch (e) {
          return null;
        }
      }
      return null;
    } catch (e) {
      return null;
    }
    return null;
  },
  getMatchedUser: async (driver, id) => {
    const session = driver.session();

    try {
      const response = await session.run(
        'match(a:Person{uuid:{id}})-[r:CHAT]->(c:Room)<-[r1:CHAT]-(b:Person) return b',
        {
          id: id
        }
      );
      if (response.records[0] === undefined) return [];
      let data = [];
      for (let user of response.records) {
        data.push({
          name: user.get(0).properties.name,
          image: user.get(0).properties.images[0],
          connected: user.get(0).properties.connected
        });
      }
      return data;
    } catch (e) {
    }
  },
  deleteRoom: async (driver, id, pseudo) => {
    const session = driver.session();

    try {
      const response = await session.run(
        'match(a:Person{name:{pseudo}}) return a.uuid',
        {
          pseudo: pseudo
        }
      );
      try {
        const response2 = await session.run(
          'match(a:Person{uuid: {id}})-[r:CHAT]->(c:Room)<-[r1:CHAT]-' +
          '(b:Person{name:{pseudo}}) DETACH DELETE c',
          {
            id: id,
            pseudo: pseudo,
            secondId: response.records[0].get(0)
          }
        );
        return true;
      } catch (e) {
        return false;
      }
    } catch (e) {
      return false;
    }
    return false;
  }
};
