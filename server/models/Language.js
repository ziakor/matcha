module.exports = {
  getLanguage: (cookies) => {
    if (typeof cookies['next-i18next'] !== 'string') {
      if (cookies['next-i18next'] === 'fr') return 'fr';
    }
    return 'en';
  }
};
