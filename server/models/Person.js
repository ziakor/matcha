var bcrypt = require('bcrypt');
const config = require('../config/index');
const relation = require('./Relationship');
const securite = require('./Secu');
const geo = require('./Geo.js');
const sort = require('./Sort');

module.exports = {
  checkParams: function (data) {
    let error = [];
    if (typeof data.firstName !== 'string' || data.firstName === '') {
      error.push('firstNameError');
    }
    if (typeof data.lastName !== 'string' || data.lastName === '') {
      error.push('lastNameError');
    }
    if (
      typeof data.pseudo !== 'string' ||
      config.regexPseudo.test(data.pseudo) === 0
    ) {
      error.push('pseudoError');
    }
    if (
      typeof data.mail !== 'string' ||
      config.regexMail.test(data.mail) === 0
    ) {
      error.push('mailError');
    }
    if (isNaN(data.age) || (!data.age >= 18 && !data.age <= 99))
      error.push('ageError');
    if (
      typeof data.passwd !== 'string' ||
      config.regexPasswd.test(data.passwd) === 0
    ) {
      error.push('passwordError');
    }
    if (data.genre !== 'H' && data.genre !== 'F') error.push('genreError');
    if (data.target !== 'H' && data.target !== 'F' && data.target !== 'B')
      error.push('targetError');
    if (data.bio !== null) {
      if (typeof data.bio !== 'string' || data.bio === '')
        error.push('bioError');
    }
    if (data.tags !== null) {
      if (typeof data.tags === 'object') {
        data.tags.map((value) => {
          if (config.tags.every((tag) => value !== tag)) {
            error.push('tagsError');
          }
        });
      } else {
        error.push('tagsError');
      }
    }
    return error;
  },
  checkImages: function (data) {
    return 1;
  },

  createPerson: async function (data, driver) {
    let merror = [];
    const session = driver.session();
    data.age = parseInt(data.age).toFixed(1);
    // Verifie si la ville est ecrit dans geo sinon -> Undifined donnera une erreur
    if (data.geo[2] === undefined) data.geo.pop();
    const resultPromise = await session.run(
      'MERGE (p:Person{name: {data}.pseudo, firstName: {data}.firstName, lastName: {data}.lastName,' +
      ' mail:{data}.mail  })ON CREATE SET p.images={data}.images, p.bio={data}.bio, p.geo={data}.geo,' +
      ' p.target={data}.target, p.images={data}.images,p.passwd={data}.passwd,p.language={data}.lang,' +
      ' p.created=true, p.confirmation={data}.confirmationkey, p.uuid={data}.uuid,p.connected="",p.age={data}.age,' +
      'p.genre={data}.genre, p.reputation=50.0 ON MATCH SET p.created=false return p.created',
      {
        data: data
      }
    );
    if (resultPromise.records[0].get(0) === false) {
      merror.push('existError');
    } else {
      await relation.likeTag(session, data);
    }
    return merror;
  },
  confirmAccount: async function (driver, pseudo, key) {
    const session = driver.session();
    const resultPromise = await session.run(
      'MATCH (p: Person{name: {pseudo}, confirmation:{key}}) SET p.confirmation="0"  return p.confirmation',
      {
        pseudo: pseudo,
        key: key
      }
    );
    return resultPromise.records[0] !== undefined;
  },
  checkConnection: async function (driver, pseudo, passwd) {
    let merror = {};
    const session = driver.session();
    const resultPromise = await session.run(
      'MATCH (p: Person{name: {pseudo} }) SET p.connected="" return p',
      {
        pseudo: pseudo
      }
    );
    if (resultPromise.records[0] === undefined) {
      merror.check = "Login Doesn't Exist";
      return merror;
    }
    let confirmation = resultPromise.records[0].get(0).properties.confirmation;
    let hasho = resultPromise.records[0].get(0).properties.passwd;
    let result = bcrypt.compareSync(passwd, '' + hasho);
    if (result && confirmation === '0') {
      merror.name = resultPromise.records[0].get(0).properties.name;
      merror.language = resultPromise.records[0].get(0).properties.language;
      merror.check = 'true';
      merror.uuid = resultPromise.records[0].get(0).properties.uuid;
      merror.language = resultPromise.records[0].get(0).properties.language;
    } else if (result && confirmation !== '0')
      merror.check = 'Account Not Confirmed';
    else merror.check = 'Invalid Password';
    return merror;
  },
  ResetverifyEmail: async function (driver, email) {
    let merror = {};
    let rand = await securite.generate(10);
    let hash = bcrypt.hashSync(rand.pass, bcrypt.genSaltSync(8), null);
    const session = driver.session();
    const resultPromise = await session.run(
      'MATCH (p: Person{mail: {email}}) SET p.passwd = {hash} return p',
      {
        email: email,
        hash: hash
      }
    );
    if (resultPromise.records[0] === undefined) merror.check = false;
    else {
      merror.check = true;
      merror.name = resultPromise.records[0].get(0).properties.firstName;
      merror.newPass = rand.pass;
    }
    return merror;
  },
  getUserData: async (driver, id) => {
    const session = driver.session();
    let tags = [];

    try {
      const response = await session.run(
        'match(p:Person{uuid: {id}}) return p',
        {
          id: id
        }
      );

      if (response.records[0] === undefined) return null;
      try {
        const response = await session.run(
          'match(p:Person{uuid: {id}})-[r:LIKE]->(t:Tags) return t',
          { id: id }
        );

        if (response.records[0] === undefined) {
          response.records[0].get(0).properties.tags = [];
        } else {
          for (value of response.records) {
            tags.push(value.get(0).properties.name);
          }
          response.records[0].get(0).properties.tags = tags;
        }
      } catch (e) {
        response.records[0].get(0).properties.tags = [];
      }

      delete response.records[0].get(0).properties.passwd;
      delete response.records[0].get(0).properties.mail;
      delete response.records[0].get(0).properties.created;
      delete response.records[0].get(0).properties.confirmation;
      delete response.records[0].get(0).properties.uuid;

      response.records[0].get(0).properties.tags = tags;
      return response.records[0].get(0).properties;
    } catch (e) {
      return 'error GetUserData';
    }
  },
  getUserMatch: async (driver, id) => {
    const session = driver.session();
    try {
      /* First lets sort by Sexual Orientation */
      let response;
      let myInfo = await session.run('match(p:Person{uuid: {id}}) return p', {
        id: id
      });
      let myImgs = myInfo.records[0].get(0).properties.images;
      const myGender = myInfo.records[0].get(0).properties.genre;
      let target = myInfo.records[0].get(0).properties.target;
      const lat1 = Number.parseInt(myInfo.records[0].get(0).properties.geo[0]);
      const lon1 = Number.parseInt(myInfo.records[0].get(0).properties.geo[1]);

      const reresponse = await session.run(
        'match(a:Person{name: {pseudo}})-[r:LOVE]->(b:Person) return b',
        {
          pseudo: myInfo.records[0].get(0).properties.name
        }
      );
      let lovetab = [];
      if (reresponse.records[0] !== undefined) {
        for (let i = 0; i < reresponse.records.length; i++) {
          lovetab.push(reresponse.records[i].get(0).properties.uuid);
        }
      }
      if (target === 'B') {
        response = await session.run(
          'WITH {lovetab} as lovetab match(p:Person) WHERE p.target="B" AND NOT p.uuid = {id} AND NOT p.uuid IN lovetab return p',
          { id: id, lovetab: lovetab }
        );
      } else {
        if (myGender === "H") {
          if (target === "H")
            target = "F"
          else
            target = "H"
        }
        else {
          if (target === "H")
            target = "H"
          else
            target = "F"
        }

        response = await session.run(
          'WITH {lovetab} as lovetab match(p:Person) WHERE NOT p.uuid = {id} AND p.genre = {target} AND NOT p.uuid IN lovetab return p',
          {
            target: target,
            id: id,
            lovetab: lovetab
          }
        );
      }
      /* END OF THIS SORT */
      /* Sort by Geolocalisation */
      let geoTab = [];
      for (let info of response.records) {
        delete info.get(0).properties.passwd;
        delete info.get(0).properties.mail;
        delete info.get(0).properties.created;
        delete info.get(0).properties.confirmation;
        delete info.get(0).properties.uuid;
        let lat2 = Number.parseInt(info.get(0).properties.geo[0]);
        let lon2 = Number.parseInt(info.get(0).properties.geo[1]);
        info.get(0).properties.distance = geo.getDistance(
          lat1,
          lon1,
          lat2,
          lon2
        );
        geoTab.push(info.get(0).properties);
      }
      geoTab = sort.triGeo(geoTab);
      const triGeo = geoTab;
      /* END OF THIS SORT */
      /* Sort By common hobbies & opularity */
      response = await session.run(
        'MATCH(p:Person{uuid: {id}})-[r:LIKE]->(t:Tags) return collect(t.name)',
        { id: id }
      );
      let MyHobbies = response.records[0].get(0);
      let theirName = [];
      for (let infos of triGeo) theirName.push(infos.name);
      const triHobbiesPop = [];
      const response2 = await session.run(
        'WITH {interets} as hobbies, {users} as listName ' +
        'MATCH (p:Person)-[r:LIKE]->(t:Tags) ' +
        'WHERE t.name in hobbies AND p.name in listName ' +
        'return p, collect(t) as tags, size(collect(t)) as numbers order by (numbers) DESC, p.reputation DESC',
        {
          interets: MyHobbies,
          users: theirName
        }
      );
      let i = 0;
      for (let info of response2.records) {
        delete info.get(0).properties.passwd;
        delete info.get(0).properties.mail;
        delete info.get(0).properties.created;
        delete info.get(0).properties.confirmation;
        delete info.get(0).properties.uuid;
        info.get(0).properties.nbComTags = info.get(2).low;
        info.get(0).properties.tags = [];
        for (let prop of info.get(1)) {
          info.get(0).properties.tags.push(prop.properties.name);
        }
        info.get(0).properties.note2 =
          ((response2.records.length - i) * 100) / response2.records.length;
        triHobbiesPop.push(info.get(0).properties);
        i++;
      }
      /* END OF THIS SORT */
      /* Then we have to get the average score of a given people */
      /* In order to get a result (Final Sort Tab) of the 2 first tab */
      /* That has been sorted previoulsy */
      const finalTab = sort.getFinalSort(triGeo, triHobbiesPop);
      const hobbiesObject = {
        MyHobbies: MyHobbies,
        MyImages: myImgs
      };
      finalTab.push(hobbiesObject);
      return finalTab;
    } catch (e) {
      return 'error triTab';
    }
  }
};
