module.exports = {
  likeTag: async (session, data) => {
    data.tags.map(async (value, index) => {
      const resultPromise = await session.run(
        'match(p:Person{name: {data}.pseudo}),(t:Tags{name: {tags}})' +
        'MERGE(p)-[r:LIKE]->(t)',
        {
          data: data,
          tags: value
        }
      );
    });
  },
  getUserData: async (session, user) => {
    try {
      const resultpromise = await session.run(
        'match(p:Person{name:{user}}) return p'
      );
      if (resultpromise.records[0] === undefined) {
        return { data: null };
      }
      return { data: resultpromise.records[0].get(0).properties };
    } catch (e) {
      return { data: null };
    }
  },
  getRelation: async (driver, id) => {
    const session = driver.session();
    try {
      const response = await session.run(
        'match(a:Person{uuid:{id}})-[r:LOVE]->(b:Person) return a,r,b',
        {
          id: id
        }
      );
      let tab = [];
      if (response.records[0]) {
        for (let i = 0; i < response.records.length; i++) {
          tab.push([response.records[i].get(2).properties.name, "LIKED BY", response.records[i].get(0).properties.name])
        }
      }
      // for (let i = 0; i < response.records.length; i++) {
      //   tab.push([
      //     response.records[i].get(0).properties.name,
      //     response.records[i].get(1).type,
      //     response.records[i].get(2).properties.name
      //   ]);
      // }
      // if (response.records[0] === undefined) return null;
      try {
        const response2 = await session.run(
          'match(a:Person)-[r:VISIT]->(b:Person{uuid:{id}}) return a,r,b',
          {
            id: id
          }
        );

        if (response2.records[0] === undefined) {
          return null;
        }

        for (let j = 0; j < response2.records.length; j++) {
          tab.push([
            response2.records[j].get(0).properties.name,
            response2.records[j].get(1).type,
            response2.records[j].get(2).properties.name
          ]);
        }
        return tab;
      } catch (e) {
        return null;
      }
    } catch (e) {
      return null;
    }
  }
};
