module.exports = {
  triGeo: (tab) => {
    let i;
    let tmp;

    i = 0;
    while (i < tab.length - 1) {
      if (tab[i].distance > tab[i + 1].distance) {
        tmp = tab[i];
        tab[i] = tab[i + 1];
        tab[i + 1] = tmp;
        i = 0;
      } else i++;
    }
    let score;
    for (i = 0; i < tab.length; i++) {
      score = ((tab.length - i) * 100) / tab.length;
      tab[i].note1 = score;
    }
    return tab;
  },
  getFinalSort: (geoTab, hobbiesTab) => {
    let finalTab = [];
    let i = 0;
    while (i < geoTab.length) {
      let j = 0;
      while (j < hobbiesTab.length) {
        if (geoTab[i].name === hobbiesTab[j].name) {
          geoTab[i].score = (geoTab[i].note1 + hobbiesTab[j].note2) / 2;
          //   geoTab[i].note2 = hobbiesTab[j].note2;
          geoTab[i].nbComTags = hobbiesTab[j].nbComTags;
          geoTab[i].tags = hobbiesTab[j].tags;
          delete geoTab[i].note1;
          finalTab.push(geoTab[i]);
        }
        j++;
      }
      i++;
    }
    i = 0;
    while (i < finalTab.length - 1) {
      if (finalTab[i].score < finalTab[i + 1].score) {
        tmp = finalTab[i];
        finalTab[i] = finalTab[i + 1];
        finalTab[i + 1] = tmp;
        i = 0;
      } else i++;
    }
    return finalTab;
  }
};
