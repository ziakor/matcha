var bcrypt = require('bcrypt');
const config = require('../config/index');
const relation = require('./Relationship');

module.exports = {
  generate: async function(l) {
    let merror = {};
    if (typeof l === 'undefined') {
      const l = 8;
    }
    /* c : chaîne de caractères alphanumérique */
    const c = 'abcdefghijknopqrstuvwxyz123456790';
    const n = c.length;
    const a = '12345679';
    const b = a.length;
    const d = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    const f = d.length;
    const p = '!@#$+-*&_';
    const o = p.length;
    let r = '';
    /* s : determine la position du caractère spécial dans le mdp */
    let s = Math.floor(Math.random() * (p.length - 1));
    for (let i = 0; i < l - 1; ++i) {
      if (s === i) {
        r += p.charAt(Math.floor(Math.random() * o));
        r += a.charAt(Math.floor(Math.random() * b));
      } else {
        if (i % 2 === 0) r += c.charAt(Math.floor(Math.random() * n));
        else r += d.charAt(Math.floor(Math.random() * f));
      }
    }
    merror.pass = r;
    return merror;
  }
};
