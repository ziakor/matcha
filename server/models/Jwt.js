var jwt = require('jsonwebtoken');
const config = require('../config/index');
const JWT_SIGN_SECRET = '123dc14AW14WDA4Wdq0wca43a4a2r5vubwsf8018y';

module.exports = {
  generateTokenForUser: function(data) {
    var res = jwt.sign(
      {
        id: data.uuid
      },
      JWT_SIGN_SECRET,
      {
        expiresIn: '4h'
      }
    );
    return res;
  },
  getUserID: function(token) {
    let userId = -1;
    if (token.length > 0) {
      try {
        const jwtToken = jwt.verify(token, JWT_SIGN_SECRET);
        userId = jwtToken.id;
      } catch (err) {
        return -1;
      }
    }
    return userId;
  }
};
