const bcrypt = require('bcrypt');

const options = [
  'Rap',
  'RnB',
  'Chiens',
  'Chats',
  'Cinema',
  'Tarantino',
  'Marvel',
  'Skateboard',
  'Traveling',
  'Paris',
  'Bordeaux',
  'Foot',
  'Coding',
  '42Born2Code',
  'Food',
  'Sport',
  'Google',
  'Beer',
  'Tuning',
  'Pringles'
];

module.exports = {
  checkPublicParams: async (data, driver) => {
    const session = driver.session();
    const regexpseudo = /^[a-zA-Z]{4,15}$/;

    let error = [];
    if (
      typeof data.pseudo !== 'string' ||
      typeof data.gender !== 'string' ||
      typeof data.sexualOrientation !== 'string'
    ) {
      return ['fatalError'];
    }

    if (data.pseudo !== '') {
      if (!regexpseudo.test(data.pseudo)) {
        error.push('pseudoError');
      } else {
        try {
          const resultPromise = await session.run(
            'MATCH (p:Person{name:{pseudo}}) return p',
            {
              pseudo: data.pseudo
            }
          );
          if (resultPromise.records[0] !== undefined) {
            error.push('pseudoError');
          }
        } catch (e) {
          error.push('pseudoError');
        }
      }
    }
    if (data.firstName !== '') {
      const firstNameRegex = /^[a-zA-Z]+$/;
      if (!firstNameRegex.test(data.firstName)) error.push('firstNameError');
    }
    if (data.lastName !== '') {
      const lastNameRegex = /^[a-zA-Z]+$/;
      if (!lastNameRegex.test(data.lastName)) error.push('lastNameError');
    }
    if (data.gender !== '') {
      if (data.gender !== 'H' && data.gender !== 'F') error.push('genderError');
    }
    if (data.sexualOrientation !== '') {
      if (
        data.sexualOrientation !== 'H' &&
        data.sexualOrientation !== 'F' &&
        data.sexualOrientation !== 'B'
      )
        error.push('sexualOrientationError');
    }
    return error;
  },

  changePublicData: async (data, driver, id) => {
    let merror = [];
    let cypherData = '';
    const session = driver.session();

    if (data.pseudo !== '') {
      cypherData = 'p.name = {data}.pseudo';
    }
    if (data.gender !== '') {
      if (cypherData !== '') {
        cypherData = `${cypherData}, p.genre = {data}.gender`;
      } else {
        cypherData = 'p.genre = {data}.gender';
      }
    }
    if (data.firstName !== '') {
      if (cypherData !== '') {
        cypherData = `${cypherData}, p.firstName = {data}.firstName`;
      } else {
        cypherData = 'p.firstName = {data}.firstName';
      }
    }
    if (data.lastName !== '') {
      if (cypherData !== '') {
        cypherData = `${cypherData}, p.lastName = {data}.lastName`;
      } else {
        cypherData = 'p.lastName = {data}.lastName';
      }
    }
    if (data.sexualOrientation !== '') {
      if (cypherData !== '') {
        cypherData = `${cypherData}, p.target = {data}.sexualOrientation`;
      } else {
        cypherData = 'p.target = {data}.sexualOrientation';
      }
    }
    const resultPromise = await session.run(
      'MATCH(p:Person{uuid : {id}}) SET ' + cypherData + ' return p',
      { data: data, id: id, cypherData: cypherData }
    );
    if (resultPromise.records[0] === undefined) {
      merror.push('fatalError');
    }
    return merror;
  },
  //PRIVATE FUNCTION
  checkPrivateParams: async (data, driver) => {
    const session = driver.session();
    const regex = /\S+@\S+\.\S+$/;
    let error = [];
    const regexpassword = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/;
    if (typeof data.mail !== 'string' || typeof data.password !== 'string') {
      error.push('fatalError');
      return error;
    }
    if (data.mail !== '') {
      if (!regex.test(data.mail)) {
        error.push('mailError');
      } else {
        try {
          let list = [];
          const resultPromise = await session.run(
            'match(n:Person{mail: {data}.mail}) return(n.name)',
            {
              data: data
            }
          );
          if (resultPromise.records[0] !== undefined) {
            error.push('mailError');
          }
        } catch (er) {
          error.push('dbError:', er);
        }
      }
    }
    if (data.password !== '') {
      if (!regexpassword.test(data.password)) {
        error.push('passwordError');
      }
    }
    session.close();
    return error;
  },
  changePrivateData: async (driver, data, id) => {
    const session = driver.session();
    let merror = [];
    let cypherdata = '';
    if (data.password !== '') {
      data.password = bcrypt.hashSync(
        data.password,
        bcrypt.genSaltSync(8),
        null
      );

      cypherdata = 'p.passwd={data}.password';
    }
    if (data.mail !== '') {
      if (cypherdata !== '') {
        cypherdata = `${cypherdata} ,p.mail={data}.mail `;
      } else {
        cypherdata = 'p.mail={data}.mail ';
      }
    }
    if (data.geo[0] !== '0' && data.geo[1] !== '0') {
      if (cypherdata !== '') {
        cypherdata = `${cypherdata} ,p.geo={data}.geo`;
      } else {
        cypherdata = 'p.geo={data}.geo';
      }
    }
    const resultPromise = await session.run(
      'MATCH(p:Person{uuid:{id}})' + ' SET ' + cypherdata + ' return(p)',
      {
        data: data,
        id: id,
        cypherdata: cypherdata
      }
    );
    if (resultPromise.records[0] === undefined) merror.push('fatalError');
    return merror;
  },
  //IMAGE
  changeImages: async (driver, images, id) => {
    const session = driver.session();
    let error = [];
    try {
      const response = await session.run(
        'match(p:Person{uuid: {id}}) SET p.images = {images} return p.images',
        {
          id: id,
          images: images
        }
      );


    } catch (e) {
      error.push('fatalError');
    }
    return error;
  },
  //TAGS
  checkTagsAndBio: async (driver, data) => {
    let error = [];
    if (typeof data.bio !== 'string' || typeof data.tags !== 'object')
      return ['fatalError'];
    data.tags.map((value) => {
      if (options.every((tag) => value !== tag)) {
        error.push('tagsError');
      }
    });
    return error;
  },

  changeTagsAndBio: async (driver, data, id) => {
    const session = driver.session();

    let error = [];

    if (data.bio !== '') {
      try {
        const resultPromise = await session.run(
          'MATCH(p:Person{uuid: {id}}) SET p.bio={data}.bio return p',
          {
            id: id,
            data: data
          }
        );

        if (resultPromise.records[0] === undefined) error.push('bioError');
      } catch (e) {
        error.push('fatalError');
      }
    }
    try {
      await session.run(
        'MATCH(p:Person{uuid: {id}})-[r:LIKE]->(t:Tags) DELETE r',
        {
          id: id
        }
      );
    } catch (e) {
      return ['fatalError'];
    }

    data.tags.map(async (value) => {
      try {
        await session.run(
          'match(p:Person{uuid: {id}}),(t:Tags{name: {tags}})' +
          'MERGE(p)-[r:LIKE]->(t)',
          {
            id: id,
            tags: value
          }
        );
      } catch (e) {
        error.push('fatalError');
        return error;
      }
    });
    return error;
  }
};
