const Jwt = require('../models/Jwt');

module.exports = (io) => {
  //!Utilitée ???
  // io.use(async (socket, next) => {
  //   const token = socket.handshake.query.tokenCookie;
  //   // console.log(socket.handshake.query.tokenCookie);
  //   if (token === undefined) {
  //     // console.log('FAIL TOKEN');
  //     return next(new Error('authentification error'));
  //   }
  //   const id = await Jwt.getUserID(token);
  //   if (id < 0) {
  //     // console.log('FAIL ID');
  //     return next(new Error('authentification error'));
  //   }
  //
  //   return next();
  // });

  //!FIN

  io.engine.generateId = (req) => {
    if (req.headers.cookie) {
      let token = req.headers.cookie.replace(
        /(?:(?:^|.*;\s*)tokenCookie\s*=\s*([^;]*).*$)|^.*$/,
        '$1'
      );

      if (token === undefined) return token;
      const id = Jwt.getUserID(token);
      if (!(id < 0)) {
        return id;
      }
    }
    return undefined;
  };

  io.on('connect', (socket) => {
    socket.on('now', (data) => {
      console.log('now', data);
    });
    socket.on('test', (data) => {
      socket.emit('test', { message: 'salsifi' });
    });
  });
  io.on('disconnect', (socket) => {
    socket.close();
  });
};
