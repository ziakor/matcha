const dotenv = require('dotenv');
dotenv.config();
/* *************** */
/* eslint-disable arrow-parens */

/* eslint-disable arrow-body-style */
/* CONFIG ET IMPORT */
const Jwt = require('./models/Jwt');
const server = require('express')();
const next = require('next');
const neo4j = require('neo4j-driver').v1;
const driver = neo4j.driver(
  'bolt://188.166.104.65',
  neo4j.auth.basic('neo4j', 'devdev123'),
  {
    encrypted: 'ENCRYPTION_OFF',
    connectionTimeout: 0,
    maxConnectionLifetime: 0
  }
);
require('./config/database')(driver);
const mailer = require('./models/Mail');
const port = process.env.PORT_API || 5000;
const dev = process.env.NODE_ENV !== 'production';
const app = next({ dev });
const handle = app.getRequestHandler();
const cors = require('cors');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const multer = require('multer');
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, `${__dirname}/../client/static/dbImages/`);
  },
  filename: function (req, file, cb) {
    var filename = file.originalname.split('.')[0];
    var fileExtension = file.mimetype.split('/')[1];
    cb(null, Date.now() + Math.floor(Math.random()) + '.' + fileExtension);
  }
});

const upload = multer({
  storage: storage,
  fileFilter: function (req, file, cb) {
    if (
      file.mimetype !== 'image/png' &&
      file.mimetype !== 'image/jpg' &&
      file.mimetype !== 'image/jpeg' &&
      file.mimetype !== 'image/JPG'
    ) {
      return cb(null, false);
    } else {
      cb(null, true);
    }
  }
});

const corsOption = {
  origin: true,
  methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
  credentials: true,
  exposedHeaders: [
    'x-auth-token',
    'Access-Control-Allow-Headers',
    'Origin',
    'Accept',
    'Content-Type',
    'Authorization'
  ]
};
const http = require('http').Server(driver, server);
const io = require('socket.io')(http, { pingTimeout: 100000 });
http.listen(5001);
require('./controller/Socket')(io);

/* ROUTE/CONFIG-EXPRESS */
/* ******************* */
app.prepare().then(() => {
  server.use(cors(corsOption));
  server.use(bodyParser.urlencoded({ extended: true }));
  server.use(bodyParser.json());
  server.use(cookieParser());

  //ROUTE SIGNUP
  require('./routes/User.js')(server, driver, upload);
  require('./routes/Settings.js')(server, driver, upload);
  require('./routes/Generate.js')(server, driver, upload);
  require('./routes/Chat')(server, driver, io, upload);
  require('./routes/Profil')(server, driver, io);
  require('./routes/Notifications')(server, driver, io);
  server.get('/posts/:id', (req, res) => {
    return app.render(req, res, '/posts', { id: req.params.id });
  });

  server.get('*', (req, res) => {
    return handle(req, res);
  });
  server.listen(port, (err) => {
    if (err) throw err;
  });
  driver.close();
});
