const settingsModel = require('../models/SettingsModels');
const jwt = require('../models/Jwt');
const geo = require('../models/Geo');
const mail = require('../models/Mail');
const secu = require('../models/Secu');
const bcrypt = require('bcrypt');
const path = require('path');
module.exports = (server, driver, upload) => {
  server.post('/settings/public', upload.array('images'), async (req, res) => {
    let merror = [];

    if (
      !req.cookies.tokenCookie ||
      typeof req.cookies.tokenCookie !== 'string'
    ) {
      return res.status(422).json({ merror: ['fatalError'] });
    }
    const id = await jwt.getUserID(req.cookies.tokenCookie);
    if (id < 0) {
      return res.status(422).json({ merror: ['fatalError'] });
    }

    merror = await settingsModel.checkPublicParams(req.body, driver);
    if (merror.length > 0) return res.status(422).json({ merror: merror });
    merror = await settingsModel.changePublicData(req.body, driver, id);
    if (merror.length > 0) return res.status(422).json({ merror: merror });

    return res.status(200).json({ merror: 'successChangePublic' });
  });

  server.post('/settings/private', upload.array('images'), async (req, res) => {
    let merror = [];
    if (
      !req.cookies.tokenCookie ||
      typeof req.cookies.tokenCookie !== 'string'
    ) {
      return res.status(422).json({ merror: ['fatalError'] });
    }
    const id = await jwt.getUserID(req.cookies.tokenCookie);
    if (id < 0) {
      return res.status(422).json({ merror: ['fatalError'] });
    }
    merror = await settingsModel.checkPrivateParams(req.body, driver);

    if (merror.length > 0) return res.status(422).json({ merror: merror });
    if (req.body.geo[0] !== '0' && req.body.geo[1] !== '1') {
      req.body.geo[2] = req.body.geo[1];
      req.body.geo[1] = req.body.geo[0];
      req.body.geo[0] = 'true';

      req.body.geo = await geo.getLocalisation(req);
      if (req.body.geo === null)
        return res.status(422).json({ merror: merror });
    }
    const ret = await settingsModel.changePrivateData(driver, req.body, id);
    merror.concat(ret);
    if (ret.length > 0) return res.status(422).json({ merror: merror });

    return res.status(200).json({ merror: merror });
  });

  server.post('/settings/images', upload.array('images'), async (req, res) => {
    let merror = [];
    if (
      !req.body.arrayImage ||
      !req.cookies.tokenCookie ||
      typeof req.cookies.tokenCookie !== 'string'
    ) {
      return res.status(422).json({ merror: ['fatalError'] });
    }
    const id = await jwt.getUserID(req.cookies.tokenCookie);
    if (id < 0) {
      return res.status(422).json({ merror: ['fatalError'] });
    }
    let data = [];

    for (let i = 0; i < 5; i++) {
      if (req.files[i]) {
        data.push(req.files[i].filename);
      } else {
        data.push(path.basename(req.body.arrayImage[i]));
      }
    }
    merror = await settingsModel.changeImages(driver, data, id);
    if (merror.length > 0) {
      return res.status(422).json({ merror: merror });
    }
    res.status(200).json({ merror: merror });
  });

  server.post('/settings/tags', upload.array('images'), async (req, res) => {

    let merror = [];
    if (
      !req.cookies.tokenCookie ||
      typeof req.cookies.tokenCookie !== 'string'
    ) {
      return res.status(422).json({ merror: ['fatalError'] });
    }
    const id = await jwt.getUserID(req.cookies.tokenCookie);
    if (id < 0) {
      return res.status(422).json({ merror: ['fatalError'] });
    }

    merror = await settingsModel.checkTagsAndBio(driver, req.body);
    if (merror.length > 0) return res.status(422).json({ merror: merror });

    merror = settingsModel.changeTagsAndBio(driver, req.body, id);
    if (merror.length > 0) return res.status(422).json({ merror: merror });
    res.status(200).json({ merror: merror });
  });
  server.get('/settings/resetPassword', async (req, res) => {
    const session = driver.session();
    if (
      !req.headers.authorization ||
      typeof req.headers.authorization !== 'string'
    ) {
      return res.status(401).end();
    }
    const id = await jwt.getUserID(req.headers.authorization);
    if (id < 0) {
      return res.status(402).end();
    }
    const newpassword = await secu.generate(10);

    const hashPassword = bcrypt.hashSync(
      newpassword.pass,
      bcrypt.genSaltSync(8),
      null
    );

    try {
      const response = await session.run(
        'match (p:Person{uuid: {id}}) SET p.passwd={password} return p',
        {
          password: hashPassword,
          id: id
        }
      );
      if (response.records[0] === undefined) return res.status(401).end();
      const user = response.records[0].get(0).properties;
      await mail.sendNewPass(
        user.mail,
        { pseudo: user.name },
        'reset Password',
        newpassword.pass
      );
    } catch (e) {
      return res.status(401).end();
    }
    return res.status(200).end();
  });
};
