const bcrypt = require('bcrypt');
const uuidv4 = require('uuid/v4');
const crypto = require('crypto');
const images = require('../models/Image.js');
const person = require('../models/Person.js');
const lang = require('../models/Language');
const mail = require('../models/Mail.js');
const Geo = require('../models/Geo.js');
const Jwt = require('../models/Jwt');
const Profil = require('../models/Profil');

module.exports = (server, driver, upload) => {
  server.post('/user/signup', upload.array('images'), async (req, res) => {
    let merror;
    if (typeof req.body.tags === 'string') req.body.tags = [req.body.tags];

    req.body.images = ['', '', '', '', ''];
    if (req.files !== undefined) {
      for (let index = 0; index < req.files.length; index++) {
        req.body.images[index] = req.files[index].filename;
      }
    }
    req.body.lang = lang.getLanguage(req.cookies);
    merror = person.checkParams(req.body);
    if (merror.length > 0) {
      images.deleteImage(req.body.images);
      return res.status(422).json({ merror: merror });
    }
    req.body.passwd = bcrypt.hashSync(req.body.passwd, bcrypt.genSaltSync(8));
    req.body.uuid = uuidv4();
    req.body.confirmationkey = crypto
      .createHash('md5')
      .update(req.body.pseudo)
      .digest('hex');

    const geo = await Geo.getLocalisation(req);
    if (geo === null) {
      return res.status(422).json({ merror: ['fatalError'] });
    } else {
      req.body.geo = geo;
    }
    const ret = await person.createPerson(req.body, driver);
    merror = merror.concat(ret);
    if (merror.length > 0) {
      // images.deleteImage(req.body.images);
      return res.status(422).json({ merror: merror });
    }

    if (merror.length === 0) {
      mail.sendmail(
        req.body.mail,
        { pseudo: req.body.pseudo },
        'Account Confirmation',
        req.body.confirmationkey
      );
    }
    return res.status(200).json({ merror: merror });
  });

  server.get('/user/confirmation', async (req, res) => {
    if (
      typeof req.query.key !== 'string' ||
      typeof req.query.pseudo !== 'string'
    )
      return res.status(301).redirect('http://localhost:3000?res=false');
    if (req.query.key === '0')
      return res.status(301).redirect('http://localhost:3000?res=false');
    const merror = await person.confirmAccount(
      driver,
      req.query.pseudo,
      req.query.key
    );
    if (merror === false)
      return res.status(301).redirect('http://localhost:3000?res=false');
    return res.status(200).redirect('http://localhost:3000/signin?res=true');
  });

  server.post('/user/signin', async (req, res) => {
    let merror = [];
    merror = await person.checkConnection(
      driver,
      req.body.mail,
      req.body.password
    );
    if (merror.check === 'true') {
      return res.status(200).json({
        merror: merror,
        token: Jwt.generateTokenForUser(merror)
      });
    } else if (merror.check === 'Account Not Confirmed') {
      return res.status(402).json({ merror: merror.check });
    } else return res.status(401).json({ merror: merror.check });
  });

  server.get('/user/checkAuth', async (req, res) => {
    var infos = {};
    if (
      !req.headers.authorization ||
      typeof req.headers.authorization !== 'string'
    ) {

      return res.status(401).json({ merror: 'Cookie Token Not Found' });
    } else {
      var userId = Jwt.getUserID(req.headers.authorization);
      if (userId < 0) {
        return res.status(401).json({ merror: 'Wrong token' });
      } else {
        return res.status(200).json({ merror: 'Valid token' });
      }
    }
  });

  server.post('/user/reset', async (req, res) => {
    let infos = {};
    let merror = [];
    merror = await person.ResetverifyEmail(driver, req.body.mail);
    if (!merror.check) return res.status(401).json({ merror: 'Invalid Email' });
    else {
      let pseudo = merror.name;
      let newPass = merror.newPass;
      mail.sendNewPass(
        req.body.mail,
        { pseudo: pseudo },
        'Password Reset',
        newPass
      );
      return res.status(200).json(infos);
    }
  });

  server.get('/user/logout', async (req, res) => {
    if (
      !req.headers.authorization ||
      typeof req.headers.authorization !== 'string'
    ) {
      return res.status(401).end();
    }
    const id = await Jwt.getUserID(req.headers.authorization);
    if (id < 0) {
      return res.status(402).end();
    }

    const session = driver.session();
    const date = Date.now().toString();

    try {
      await session.run(
        'match(p:Person{uuid: {id}}) SET p.connected={date} return p',
        {
          id: id,
          date: date
        }
      );
      session.close();
      return res.status(200).end();
    } catch (e) {
      return res.status(401).end();
    }
  });

  server.get('/user/matchs', async (req, res) => {
    if (
      !req.headers.authorization ||
      typeof req.headers.authorization !== 'string'
    ) {
      return res.status(422).json({ merror: null });
    }
    const id = await Jwt.getUserID(req.headers.authorization);
    if (id < 0) {
      return res.status(422).json({ merror: ['fatalError'] });
    }
    const data = await person.getUserMatch(driver, id);
    return res.status(200).json({ data: data });
  });
};
