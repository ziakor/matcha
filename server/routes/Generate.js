const bcrypt = require('bcrypt');
const uuidv4 = require('uuid/v4');
const images = require('../models/Image.js');
const faker = require('faker');
const Secu = require('../models/Secu');
const Relation = require('../models/Relationship');
faker.seed(Math.floor(Math.random() * 9999 + 1));

const options = [
  'Rap',
  'RnB',
  'Chiens',
  'Chats',
  'Cinema',
  'Tarantino',
  'Marvel',
  'Skateboard',
  'Traveling',
  'Paris',
  'Bordeaux',
  'Foot',
  'Coding',
  '42Born2Code',
  'Food',
  'Sport',
  'Google',
  'Beer',
  'Tuning',
  'Pringles'
];

const langueList = ['en', 'fr'];
const sexualOrientationList = ['H', 'F', ' B'];
const genderList = ['H', 'F'];
module.exports = (server, driver, upload) => {
  //ADD IMAGES TO SERVER
  //ADD PASSWORD
  // ADD GEO
  // ADD AGE
  // ADD GENRE / SEXUALORIENTATION
  // ADD LANGUAGE
  server.get(
    '/generateprofile/:nb',
    upload.array('images'),
    async (req, res) => {
      const session = driver.session();
      const nb = parseInt(req.params.nb);
      let i = 0;
      let j = 0;
      let list = [];
      const pathimages = `${__dirname}/../../client/static/dbImages/`;

      while (i < nb) {
        const gender = genderList[faker.random.number({ min: 0, max: 1 })];
        const language = langueList[faker.random.number({ min: 0, max: 1 })];
        const sexualOr =
          sexualOrientationList[faker.random.number({ min: 0, max: 2 })];
        let password = await Secu.generate();
        password = bcrypt.hashSync(password.pass, bcrypt.genSaltSync(8));

        const age = faker.random.number({ min: 18, max: 99 });

        const imagesData = [
          '' + Date.now() + Math.random() + '.jpg',
          '' + Date.now() + Math.random() + '.jpg',
          '' + Date.now() + Math.random() + '.jpg',
          '' + Date.now() + Math.random() + '.jpg',
          '' + Date.now() + Math.random() + '.jpg'
        ];

        imagesData.map(async (val, index) => {
          if (index === 0) {
            await images.saveImageToDisk(
              'https://thispersondoesnotexist.com/image',
              `${pathimages}${val}`
            );
          } else {
            await images.saveImageToDisk(
              'https://lorempixel.com/640/480/',
              `${pathimages}${val}`
            );
          }
        });
        //
        list[i] = {
          name: faker.internet.userName(),
          firstName: faker.name.firstName(gender === 'H' ? 0 : 1),
          lastName: faker.name.lastName(gender === 'H' ? 0 : 1),
          mail: faker.internet.email(),
          genre: gender,
          target: sexualOr,
          uuid: uuidv4(),
          language: language,
          bio: faker.lorem.sentence(),
          geo: [
            faker.address.longitude(),
            faker.address.latitude(),
            faker.address.city()
          ],
          images: [
            imagesData[0],
            imagesData[1],
            imagesData[2],
            imagesData[3],
            imagesData[4]
          ],
          passwd: password,
          age: age,
          confirmation: '0',
          created: 'true',
          reputation: faker.random.number({ min: 1, max: 100 })
        };
        try {
          await session.run(
            'MERGE (p:Person{name:{data}.name, firstName: {data}.firstName, ' +
            'lastName: {data}.lastName, mail: {data}.mail, genre: {data}.genre, target: {data}.target, ' +
            'uuid: {data}.uuid, language: {data}.language, bio: {data}.bio, geo: {data}.geo ,' +
            'images : {data}.images, passwd: {data}.passwd, age: {data}.age, ' +
            'confirmation: {data}.confirmation, created: {data}.created , reputation: {data}.reputation}) return(p)',
            {
              data: list[i]
            }
          );

          // LIKE TAGS
          const nbTags = Math.floor(Math.random() * 20 + 1);
          let tagsUser = [];
          let compteur = 0;
          while (compteur < nbTags) {
            const tagchoice = Math.floor(Math.random() * 20 + 1) - 1;
            tagsUser.push(options[tagchoice]);
            compteur++;
          }
          tagsUser.map(async (value, index) => {
            try {
              await session.run(
                'match(p:Person{name: {data}.name}),(t:Tags{name: {tags}})' +
                'MERGE(p)-[r:LIKE]->(t)',
                {
                  data: list[i],
                  tags: value
                }
              );
            } catch (err) { }
          });
        } catch (err) {
        }
        i++;
      }
      i = 0;
      while (i < nb) {
        let nblove = faker.random.number({ min: 1, max: nb });
        let idlove = [];
        while (nblove > 0) {
          let tentative = 1;

          let id = faker.random.number({ min: 0, max: nb - 1 });
          //AJOUTER UNE VERIFICATION DU GENRE ET DE LORIENTATION SEXUELLE
          while (id === i) {
            id = faker.random.number({ min: 0, max: nb - 1 });
            tentative++;
            if (tentative >= nb) break;
          }
          if (tentative >= nb) break;

          idlove.push(id);
          nblove--;
        }

        for (let h = 0; h < idlove.length; h++) {
          const next = list[idlove[h]];
          const current = list[i];

          try {
            await session.run(
              'match(a:Person{name:{current}.name}), (b:Person{name: {next}.name}) ' +
              'merge (a)-[r:LOVE]->(b)',
              {
                current: current,
                next: next
              }
            );
          } catch (e) {
          }
        }
        i++;
      }
      session.close();
      res.status(200).end();
    }
  );
};
