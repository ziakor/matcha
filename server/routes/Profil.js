const bcrypt = require('bcrypt');
const uuidv4 = require('uuid/v4');
const crypto = require('crypto');
const images = require('../models/Image.js');
const person = require('../models/Person.js');
const lang = require('../models/Language');
const relation = require('../models/Relationship');
const mail = require('../models/Mail.js');
const Geo = require('../models/Geo.js');
const Jwt = require('../models/Jwt');
const Profil = require('../models/Profil');

module.exports = (server, driver, io) => {
  server.get('/profil/myinfos', async (req, res) => {
    if (
      !req.headers.authorization ||
      typeof req.headers.authorization !== 'string'
    ) {
      return res.status(422).json({ merror: null });
    }
    const id = await Jwt.getUserID(req.headers.authorization);
    if (id < 0) {
      return res.status(422).json({ merror: ['fatalError'] });
    }
    const data = await person.getUserData(driver, id);
    return res.status(200).json({ data: data });
  });
  server.get('/profil/:pseudo', async (req, res) => {
    if (
      !req.headers.authorization ||
      typeof req.headers.authorization !== 'string'
    ) {
      return res.status(424).json({ data: null });
    }
    const id = await Jwt.getUserID(req.headers.authorization);
    if (id < 0) {
      return res.status(424).json({ data: null });
    }
    try {
      const session = driver.session();
      const result0 = await session.run(
        'match(p:Person{uuid: {id}}) return p',
        {
          id: id
        }
      );
      if (
        result0.records &&
        result0.records[0].get(0).properties.name === req.params.pseudo
      ) {
        return res.status(422).json({ data: null });
      }

      const result = session.run(
        'match(a:Person{uuid: {id}})-[r:BLOCK]->' +
        '(b:Person{name:{pseudo}}) return a, a.name',
        {
          id: id,
          pseudo: req.params.pseudo
        }
      );
      if (result.records !== undefined) {
        return res.status(422).json({ data: null });
      }
      session.close();
    } catch (e) {
      return res.status(422).json({ data: null });
    }

    const data = await Profil.getDataOtherProfil(driver, req.params.pseudo, id);

    if (data === null) return res.status(424).json({ data: null });
    const visit = await Profil.visitProfile(driver, req.params.pseudo, id, io);

    if (visit === false) return res.status(401).json({ data: null });

    return res.status(200).json({ data: data });
  });
  server.get('/profil/like/:pseudo', async (req, res) => {
    if (
      !req.headers.authorization ||
      typeof req.headers.authorization !== 'string'
    ) {
      return res.status(401).end();
    }
    const id = await Jwt.getUserID(req.headers.authorization);
    if (id < 0) {
      return res.status(402).end();
    }
    const data = await Profil.likeProfil(driver, req.params.pseudo, id, io);
    if (data === -1) return res.status(424).end();

    return res.status(200).json({ ret: data });
  });
  server.get('/profil/block/:pseudo', async (req, res) => {
    if (
      !req.headers.authorization ||
      typeof req.headers.authorization !== 'string'
    ) {
      return res.status(401).end();
    }
    const id = await Jwt.getUserID(req.headers.authorization);
    if (id < 0) {
      return res.status(402).end();
    }
    const data = Profil.blockProfil(driver, req.params.pseudo, id);

    if (data === false) return res.status(402).end();
    return res.status(200).end();
  });
  server.get('/profil/report/:pseudo', async (req, res) => {
    if (
      !req.headers.authorization ||
      typeof req.headers.authorization !== 'string'
    ) {
      return res.status(401).end();
    }
    const id = await Jwt.getUserID(req.headers.authorization);
    if (id < 0) {
      return res.status(402).end();
    }
    const data = await Profil.reportProfil(driver, req.params.pseudo, id);
    if (data === false) return res.status(402).end();

    return res.status(200).end();
  });

  server.get('/myprofil/history', async (req, res) => {
    if (
      !req.headers.authorization ||
      typeof req.headers.authorization !== 'string'
    ) {
      return res.status(401).end();
    }
    const id = await Jwt.getUserID(req.headers.authorization);
    if (id < 0) {
      return res.status(402).end();
    }
    const data = await relation.getRelation(driver, id);
    if (data === null) return res.status(424).end();
    return res.status(200).json({ data });
  });
};
