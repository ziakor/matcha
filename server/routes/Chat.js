const Chat = require('../models/Chat');
const Jwt = require('../models/Jwt');

module.exports = (server, driver, io, upload) => {
  server.post('/chat/addMessage', upload.array('images'), async (req, res) => {
    if (
      !req.cookies.tokenCookie ||
      typeof req.cookies.tokenCookie !== 'string'
    ) {
      return res.status(401).end();
    }
    const id = await Jwt.getUserID(req.cookies.tokenCookie);
    if (id < 0) {
      return res.status(401).end();
    }
    if (req.body.pseudo === '' || req.body.message === '')
      return res.status(422).end();
    const data = await Chat.addMessage(
      driver,
      id,
      req.body.pseudo,
      req.body.message,
      io
    );
    if (data === false) {
      return res.status(403).end();
    }
    return res.status(200).end();
  });

  server.get('/chat/getRoom/:pseudo', async (req, res) => {
    if (typeof req.params.pseudo !== 'string' || req.params.pseudo === '')
      return res.status(401).end();
    if (
      !req.headers.authorization ||
      typeof req.headers.authorization !== 'string'
    ) {
      return res.status(401).end();
    }
    const id = await Jwt.getUserID(req.headers.authorization);
    if (id < 0) {
      return res.status(401).end();
    }
    const data = await Chat.getRoom(driver, id, req.params.pseudo);
    if (data === null) {
      return res.status(424).end();
    }
    return res.status(200).json(data);
  });
  server.get('/chat/getMatchUser', async (req, res) => {
    if (
      !req.headers.authorization ||
      typeof req.headers.authorization !== 'string'
    ) {
      return res.status(401).end();
    }
    const id = await Jwt.getUserID(req.headers.authorization);
    if (id < 0) {
      return res.status(401).end();
    }
    const data = await Chat.getMatchedUser(driver, id);
    if (data === null) {
      return res.status(424).end();
    }
    return res.status(200).json({ data: data });
  });
};
