const bcrypt = require('bcrypt');
const uuidv4 = require('uuid/v4');
const crypto = require('crypto');
const images = require('../models/Image.js');
const person = require('../models/Person.js');
const lang = require('../models/Language');
const relation = require('../models/Relationship');
const mail = require('../models/Mail.js');
const Geo = require('../models/Geo.js');
const Jwt = require('../models/Jwt');
const Profil = require('../models/Profil');
const Notifications = require('../models/NotificationsModel');
module.exports = (server, driver, io) => {
  server.put('/notifications/delete', async (req, res) => {
    if (
      !req.cookies.tokenCookie ||
      typeof req.cookies.tokenCookie !== 'string'
    ) {
      return res.status(401).end();
    }
    const id = await Jwt.getUserID(req.cookies.tokenCookie);
    if (id < 0) {
      return res.status(402).end();
    }
    await Notifications.removeActiveNotifications(driver, id);
    return res.status(200).end();
  });

  server.get('/notifications/get', async (req, res) => {
    if (
      !req.headers.authorization ||
      typeof req.headers.authorization !== 'string'
    ) {
      return res.status(200).json({ data: [] });
    }
    const id = await Jwt.getUserID(req.headers.authorization);
    if (id < 0) {
      return res.status(424).json({ data: [] });
    }
    const data = await Notifications.getNotifications(driver, id);
    if (data === null) return res.status(424).json({ data: [] });

    io.emit();
    return res.status(200).json({ data: data });
  });
};
